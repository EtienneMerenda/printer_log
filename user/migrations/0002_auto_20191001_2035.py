# Generated by Django 2.2.5 on 2019-10-01 18:35

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='last_login',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Dernière connexion'),
        ),
        migrations.AddField(
            model_name='user',
            name='password',
            field=models.CharField(default=1, max_length=100, verbose_name='Mot de passe'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='user',
            name='signin_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name="Date d'inscription"),
        ),
        migrations.AlterField(
            model_name='user',
            name='name',
            field=models.CharField(max_length=100, unique=True, verbose_name='Nom'),
        ),
    ]
