# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm
# import Printer and Settings for table relation
from printer.models.printer import Printer
from cura_settings.models.settings import Settings

from django.utils import timezone

# Create your models here.
class User(models.Model):

    # Store user's name
    name = models.CharField(
        max_length=100,
        verbose_name='Nom',
        unique=True
        )

    # Store user's password
    password = models.CharField(
        max_length=100,
        verbose_name='Mot de passe'
    )

    # Store all printers of user
    printer = models.ManyToManyField(
        Printer,
        verbose_name='Imprimante 3D'
    )

    # Store all settings used by user in his different impressions
    settings = models.ManyToManyField(
        Settings,
        verbose_name='Réglages'
    )

    signin_date = models.DateField(
        'Date d\'inscription',
        default=timezone.now
    )

    last_login = models.DateField(
        'Dernière connexion',
        default=timezone.now
    )


class Userform(ModelForm):
    class Meta:
        model = User
        fields = [
            'name',
            'printer',
            'settings'
        ]
