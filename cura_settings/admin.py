from django.contrib import admin
from .models import (Blackmagic, Cooling, Dual, Experimental, Infill, Settings,
    Material, Platform_adhesion, Quality, Shell, Speed, Support, Travel)

# Register your models here.
admin.site.register(Blackmagic)
admin.site.register(Cooling)
admin.site.register(Dual)
admin.site.register(Experimental)
admin.site.register(Infill)
admin.site.register(Material)
admin.site.register(Platform_adhesion)
admin.site.register(Quality)
admin.site.register(Shell)
admin.site.register(Speed)
admin.site.register(Support)
admin.site.register(Travel)
admin.site.register(Settings)
