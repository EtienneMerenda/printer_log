# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm
# import Extruder for select extruder used in multiple extruders case
from printer.models.extruder import Extruder


class Shell(models.Model):
    fills = (("A", "Partout"),
             ("N", "Nulle part"),)

    wall_extruder_nr = models.ForeignKey(
        Extruder,
        verbose_name="Extrudeuse de paroi",
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='wall_extruder_nr'
    )

    wall_0_extruder_nr = models.ForeignKey(
        Extruder,
        verbose_name="Extrudeuse de paroi externe",
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='wall_0_extruder_nr'
    )

    wall_x_extruder_nr = models.ForeignKey(
        Extruder,
        verbose_name="Extrudeuse de paroi interne",
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='wall_x_extruder_nr'
    )

    wall_thickness = models.FloatField(
        verbose_name="Epaisseur de la paroi")

    wall_line_count = models.IntegerField(
        verbose_name="Nombre de ligne de la paroi",
    )

    top_bottom_extruder_nr = models.ForeignKey(
        Extruder,
        verbose_name="Extrudeuse du dessus/base",
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='top_bottom_extruder_nr'
    )

    top_bottom_thickness = models.FloatField(
        verbose_name="Epaisseur du dessus/base",
    )

    top_thickness = models.FloatField(
        verbose_name="Epaisseur du dessus",
    )

    top_layers = models.IntegerField(
        verbose_name="Nombre de couches supérieures",
    )

    bottom_thickness = models.FloatField(
        verbose_name="Epaisseur de la base",
    )

    bottom_layers = models.IntegerField(
        verbose_name="Nombre de couches inferieur",
    )

    optimize_wall_printing_order = models.BooleanField(
        verbose_name="Optimiser l'orde d'impression des parois",
    )

    fill_perimeter_gaps = models.CharField(
        max_length=100,
        verbose_name="Remplir les trous entre les parois",
        choices=fills
    )

    xy_offset = models.FloatField(
        verbose_name="Vitesse d'impression horizontale",
    )

    ironing_enabled = models.BooleanField(
        verbose_name="Activer l'étirage"
    )


class ShellForm(ModelForm):
    class Meta:
        model = Shell
        fields = [
            'wall_extruder_nr',
            'wall_0_extruder_nr',
            'wall_x_extruder_nr',
            'wall_thickness',
            'wall_line_count',
            'top_bottom_extruder_nr',
            'top_bottom_thickness',
            'top_thickness',
            'top_layers',
            'bottom_thickness',
            'bottom_layers',
            'optimize_wall_printing_order',
            'fill_perimeter_gaps',
            'xy_offset',
            'ironing_enabled'
        ]

    # format:

    # wall_extruder_nr
    #     wall_0_extruder_nr
    #     wall_x_extruder_nr
    # wall_thickness
    #     wall_line_count
    # top_bottom_extruder_nr
    # top_bottom_thickness
    #     top_thickness
    #         top_layers
    #     bottom_thickness
    #         bottom_layers
    # optimize_wall_printing_order
    # fill_perimeter_gaps
    # xy_offset
    # ironing_enabled
