# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm
from .validators.validate_percent import validate_percent


from .blackmagic import Blackmagic, BlackmagicForm
from .cooling import Cooling, CoolingForm
from .dual import Dual, DualForm
from .experimental import Experimental, ExperimentalForm
from .infill import Infill, InfillForm
from .material import Material, MaterialForm
from .platform_adhesion import Platform_adhesion, Platform_adhesionForm
from .quality import Quality, QualityForm
from .shell import Shell, ShellForm
from .speed import Speed, SpeedForm
from .support import Support, SupportForm
from .travel import Travel, TravelForm
from .settings import Settings, SettingsForm
