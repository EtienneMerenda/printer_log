# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Material(models.Model):

    material_print_temperature = models.IntegerField(
        verbose_name="Température d'impression"
    )

    material_print_temperature_layer_0 = models.IntegerField(
        verbose_name="Température d'impression de la couche initiale"
    )

    material_initial_print_temperature = models.IntegerField(
        verbose_name="Température d'impression initiale"
    )

    material_final_print_temperature = models.IntegerField(
        verbose_name="Température d'impression finale"
    )

    material_bed_temperature = models.IntegerField(
        verbose_name="Température du plateau"
    )

    material_bed_temperature_layer_0 = models.IntegerField(
        verbose_name="Température du plateau pendant la couche initiale"
    )

    retraction_enable = models.BooleanField(
        verbose_name="Activer la rétractation",
    )

    retract_at_layer_change = models.BooleanField(
        verbose_name="Rétracter au changement de couche",
    )

    retraction_amount = models.FloatField(
        verbose_name="Distance de rétractation"
    )

    retraction_speed = models.FloatField(
        verbose_name="Vitesse de rétractation"
    )

    material_standby_temperature = models.IntegerField(
        verbose_name="Température de veille"
    )


class MaterialForm(ModelForm):
    class Meta:
        model = Material
        fields = [
            'material_print_temperature',
            'material_print_temperature_layer_0',
            'material_initial_print_temperature',
            'material_final_print_temperature',
            'material_bed_temperature',
            'material_bed_temperature_layer_0',
            'retraction_enable',
            'retract_at_layer_change',
            'retraction_amount',
            'retraction_speed',
            'material_standby_temperature'
        ]

    # format:

    # material_print_temperature
    # material_print_temperature_layer_0
    # material_initial_print_temperature
    # material_final_print_temperature
    # material_bed_temperature
    # material_bed_temperature_layer_0
    # retraction_enable
    # retract_at_layer_change
    # retraction_amount
    # retraction_speed
    # material_standby_temperature
