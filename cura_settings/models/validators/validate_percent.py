from django.forms import ValidationError


def validate_percent(value):
    try:
        value = int(value)
        if value <= 100 and value >= 0:
            return value
        else:
            raise ValidationError("La valeur n'est pas un entier compris entre "
                                  "0 et 100")
    except ValueError:
        raise ValidationError("La valeur n'est pas un entier compris entre "
                              "0 et 100")
