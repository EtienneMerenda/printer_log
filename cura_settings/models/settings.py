# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm

from ..models import (
    Blackmagic,
    Cooling,
    Dual,
    Experimental,
    Infill,
    Material,
    Platform_adhesion,
    Quality,
    Shell,
    Speed,
    Support,
    Travel
)

class Settings(models.Model):

    label = models.CharField(
        verbose_name="Label",
        max_length=100
    )

    blackmagic = models.ForeignKey(
        Blackmagic,
        verbose_name='Modes spéciaux',
        on_delete=models.SET_NULL,
        null=True
    )

    cooling = models.ForeignKey(
        Cooling,
        verbose_name='Refroidissment',
        on_delete=models.SET_NULL,
        null=True
    )

    dual = models.ForeignKey(
        Dual,
        verbose_name='Double Extrusion',
        on_delete=models.SET_NULL,
        null=True
    )

    experimental = models.ForeignKey(
        Experimental,
        verbose_name='Expérimental',
        on_delete=models.SET_NULL,
        null=True
    )

    infill = models.ForeignKey(
        Infill,
        verbose_name='Remplissage',
        on_delete=models.SET_NULL,
        null=True
    )

    material = models.ForeignKey(
        Material,
        verbose_name='Matérieaux',
        on_delete=models.SET_NULL,
        null=True
    )

    platform_adhesion = models.ForeignKey(
        Platform_adhesion,
        verbose_name='Adhérence du plateau',
        on_delete=models.SET_NULL,
        null=True
    )

    quality = models.ForeignKey(
        Quality,
        verbose_name='Qualité',
        on_delete=models.SET_NULL,
        null=True
    )

    shell = models.ForeignKey(
        Shell,
        verbose_name='Coque',
        on_delete=models.SET_NULL,
        null=True
    )

    speed = models.ForeignKey(
        Speed,
        verbose_name='Vitesse',
        on_delete=models.SET_NULL,
        null=True
    )

    support = models.ForeignKey(
        Support,
        verbose_name='Supports',
        on_delete=models.SET_NULL,
        null=True
    )

    travel = models.ForeignKey(
        Travel,
        verbose_name='Déplacement',
        on_delete=models.SET_NULL,
        null=True
    )

    def __str__(self):

        label = self.label

        return label


class SettingsForm(ModelForm):
    class Meta:
        model = Settings
        fields = [
                'blackmagic',
                'cooling',
                'dual',
                'experimental',
                'infill',
                'material',
                'platform_adhesion',
                'quality',
                'shell',
                'speed',
                'support',
                'travel'
                ]
