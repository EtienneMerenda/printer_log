# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm
# import validate_percent function who validate posted value
from .validators.validate_percent import validate_percent

# Quality settings table


class Quality(models.Model):

    layer_height = models.FloatField(
        verbose_name="Hauteur de la couche"
    )

    layer_height_0 = models.FloatField(
        verbose_name="Hauteur de la couche initiale"
    )

    line_width = models.FloatField(
        verbose_name="Largeur de ligne"
    )

    wall_line_width = models.FloatField(
        verbose_name="Largeur de ligne de la paroi"
    )

    wall_line_width_0 = models.FloatField(
        verbose_name="Largeur de ligne de la paroi externe",
        blank=True
    )

    wall_line_width_x = models.FloatField(
        verbose_name="Largeur de ligne de la paroi interne",
        blank=True
    )

    top_bottom_line_width = models.FloatField(
        verbose_name="Largeur de la ligne du dessus/dessous",
        blank=True
    )

    infill_line_width = models.FloatField(
        verbose_name="Largeur de la ligne de remplissage",
        blank=True
    )

    initial_layer_line_width_factor = models.IntegerField(validators=(validate_percent,),
                                                          verbose_name="Largeur de la ligne de couche initiale",
                                                          blank=True
                                                          )


class QualityForm(ModelForm):
    class Meta:
        model = Quality
        fields = [
            'layer_height',
            'layer_height_0',
            'line_width',
            'wall_line_width',
            'wall_line_width_0',
            'wall_line_width_x',
            'top_bottom_line_width',
            'infill_line_width',
            'initial_layer_line_width_factor'
        ]

    # format:

        # layer_height: value (float)
        # layer_height_0: value (float)
        # line_width: value (float)
        #     wall_line_width: value (float)
        #         wall_line_width_0: value (float)
        #         wall_line_width_x: value (float)
        #     top_bottom_line_width: value (float)
        #     infill_line_width: value (float)
        # initial_layer_line_width_factor (integer)
