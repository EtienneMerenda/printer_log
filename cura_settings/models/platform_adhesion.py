# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm
# import Extruder for select extruder used in several extruders case
from printer.models.extruder import Extruder



class Platform_adhesion(models.Model):

    prime_blob_enable = models.BooleanField(
        verbose_name="Activer la goutte de préparation"
    )

    adhesion_type = models.CharField(
        max_length=100,
        verbose_name="Type d'adhérence du plateau",
        choices=(("J", "Jupe"),
                 ("B", "Bordure"),
                 ("R", "Radeau"),
                 ("N", "Aucun"),
                 )
    )

    adhesion_extruder_nr = models.ForeignKey(
        Extruder,
        verbose_name="Extrudeuse d'adhérence du plateau",
        on_delete=models.SET_DEFAULT,
        default=1
    )

    skirt_line_count = models.IntegerField(
        verbose_name="Nombre de ligne de la jupe"
    )

    brim_width = models.FloatField(
        verbose_name="Largeur de la bordure"
    )

    brim_line_count = models.IntegerField(
        verbose_name="Nombre de ligne de la bordure"
    )

    brim_outside_only = models.BooleanField(
        verbose_name="Bordure uniquement sur l'extérieur"
    )


class Platform_adhesionForm(ModelForm):
    class Meta:
        model = Platform_adhesion
        fields = [
            'prime_blob_enable',
            'adhesion_type',
            'adhesion_extruder_nr',
            'skirt_line_count',
            'brim_width',
            'brim_line_count',
            'brim_outside_only'
        ]

    # format:

    # prime_blob_enable
    # adhesion_type
    # adhesion_extruder_nr
    # skirt_line_count
    # brim_width
    #     brim_line_count
    # brim_outside_only
