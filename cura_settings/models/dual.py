# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Dual(models.Model):

    prime_tower_enable = models.BooleanField(
        verbose_name="Activer la tour primaire",
    )

    prime_tower_position_x = models.FloatField(
        verbose_name="Position X de la tour primaire"
    )

    prime_tower_position_y = models.FloatField(
        verbose_name="Position Y de la tour primaire"
    )

    prime_tower_brim_enable = models.BooleanField(
        verbose_name="Bordure de la tour primaire",
    )


class DualForm(ModelForm):
    class Meta:
        model = Dual
        fields = [
            'prime_tower_enable',
            'prime_tower_position_x',
            'prime_tower_position_y',
            'prime_tower_brim_enable'
        ]

    # format:

    # prime_tower_enable
    # prime_tower_position_x
    # prime_tower_position_y
    # prime_tower_brim_enable
