# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Experimental(models.Model):

    conical_overhang_enabled = models.BooleanField(
        verbose_name="Rendre le porte-à-faux imprimable",
    )

    support_conical_enabled = models.BooleanField(
        verbose_name="Activer les supports coniques",
    )

    adaptive_layer_height_enabled = models.BooleanField(
        verbose_name="Utiliser les couches adaptatives",
    )

    adaptive_layer_height_variation = models.FloatField(
        verbose_name="Variation maximale des couches adaptatives"
    )
    adaptive_layer_height_variation_step = models.FloatField(
        verbose_name="Taille des étapes de varaition des couches adaptatives"
    )
    adaptive_layer_height_threshold = models.FloatField(
        verbose_name="Limites de cousches adaptatives"
    )


class ExperimentalForm(ModelForm):
    class Meta:
        model = Experimental
        fields = [
            'conical_overhang_enabled',
            'support_conical_enabled',
            'adaptive_layer_height_enabled',
            'adaptive_layer_height_variation',
            'adaptive_layer_height_variation_step',
            'adaptive_layer_height_threshold'
        ]

    # format:

    # conical_overhang_enabled
    # support_conical_enabled
    # adaptive_layer_height_enabled
    #     adaptive_layer_height_variation
    #     adaptive_layer_height_variation_step
    #     adaptive_layer_height_threshold
