# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm
# import Extruder for select extruder used in multiple extruders case
from printer.models.extruder import Extruder
# import validate_percent function who validate posted value
from .validators.validate_percent import validate_percent



class Support(models.Model):

    support_enable = models.BooleanField(
        verbose_name="Générer les supports"
    )

    support_extruder_nr = models.ForeignKey(
        Extruder,
        verbose_name="Extrudeuse de support",
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='support_extruder_nr'
    )

    support_infill_extruder_nr = models.ForeignKey(
        Extruder,
        verbose_name="Extrudeuse de remplissage du support",
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='support_infill_extruder_nr'
    )

    support_extruder_nr_layer_0 = models.ForeignKey(
        Extruder,
        verbose_name="Extrudeuse de support de première couche",
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='support_extruder_nr_layer_0'
    )

    support_interface_extruder_nr = models.ForeignKey(
        Extruder,
        verbose_name="Extrudeuse de l'interface du support",
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='support_interface_extruder_nr'
    )

    support_type = models.CharField(
        max_length=100,
        verbose_name="Positionnement des supports",
        choices=(("Partout", "Partout"),
                 ("contact", "En contact avec le plateau"))
    )

    support_angle = models.IntegerField(
        verbose_name="Angle de porte-à-faux du support",
        validators=(validate_percent,)
    )

    support_pattern = models.CharField(
        max_length=100,
        verbose_name="Motif du support",
        choices=(("L", "Lignes"),
                 ("G", "Grille"),
                 ("T", "Triangles"),
                 ("C", "Concentrique"),
                 ("Z", "Zig Zag"),
                 ("E", "Entrecroisé"),
                 ("G", "Gyroïde"),
                 )
    )

    support_infill_rate = models.IntegerField(
        verbose_name="Densité du support",
        validators=(validate_percent,)
    )

    support_offset = models.FloatField(
        verbose_name="Expansion horizontale des supports"
    )

    support_infill_sparse_thickness = models.FloatField(
        verbose_name="Epaisseur de la couche de remplissage de support"
    )

    gradual_support_infill_steps = models.FloatField(
        verbose_name="Etapes de remplissage graduel du support"
    )

    gradual_support_infill_step_height = models.FloatField(
        verbose_name="Hauteur d'étapes de remplissage graduel du support"
    )

    support_interface_enable = models.BooleanField(
        verbose_name="Activer l'interface de support"
    )

    support_roof_enable = models.BooleanField(
        verbose_name="Activer les plafonds de support"
    )

    support_bottom_enable = models.BooleanField(
        verbose_name="Activer les bas de support"
    )


class SupportForm(ModelForm):
    class Meta:
        model = Support
        fields = [
            'support_enable',
            'support_extruder_nr',
            'support_infill_extruder_nr',
            'support_extruder_nr_layer_0',
            'support_interface_extruder_nr',
            'support_type',
            'support_angle',
            'support_pattern',
            'support_infill_rate',
            'support_offset',
            'support_infill_sparse_thickness',
            'gradual_support_infill_steps',
            'gradual_support_infill_step_height',
            'support_interface_enable',
            'support_roof_enable',
            'support_bottom_enable',
        ]

    # format:

    # support_enable
    # support_extruder_nr
    #     support_infill_extruder_nr
    #     support_extruder_nr_layer_0
    #     support_interface_extruder_nr
    # support_type
    # support_angle
    # support_pattern
    # support_infill_rate
    # support_offset
    # support_infill_sparse_thickness
    # gradual_support_infill_steps
    # gradual_support_infill_step_height
    # support_interface_enable
    #     support_roof_enable
    #     support_bottom_enable
