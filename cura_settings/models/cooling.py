# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm
# import validate_percent function who validate posted value 
from .validators.validate_percent import validate_percent


class Cooling(models.Model):

    cool_fan_enabled = models.BooleanField(
        verbose_name="Activer le refroidissement de l'impression",
        default=True
    )

    cool_fan_speed = models.IntegerField(
        verbose_name="Vitesse du ventilateur",
        validators=(validate_percent,)
    )

    cool_fan_speed_min = models.IntegerField(
        verbose_name="Vitesse régulière du ventilateur",
        validators=(validate_percent,)
    )

    cool_fan_speed_max = models.IntegerField(
        verbose_name="Vitesse maximale du ventilateur",
        validators=(validate_percent,)
    )

    cool_min_layer_time_fan_speed_max = models.IntegerField(
        verbose_name="Limite de vitesse rédulière/maximale du ventilateur"
    )

    cool_fan_speed_0 = models.IntegerField(
        verbose_name="Vitesse du ventilateurs initiale",
        validators=(validate_percent,)
    )

    cool_fan_full_at_height = models.FloatField(
        verbose_name="Vitesse régulière du ventilateur à la hauteur",
    )

    cool_fan_full_layer = models.FloatField(
        verbose_name="Vitesse du ventilateur à la couche",
    )

    cool_min_layer_time = models.FloatField(
        verbose_name="Durée minimale d'une couche",
    )

    cool_min_speed = models.FloatField(
        verbose_name="Vitesse minimale",
    )

    cool_lift_head = models.BooleanField(
        verbose_name="Relever la tête",
    )


class CoolingForm(ModelForm):
    class Meta:
        model = Cooling
        fields = [
            'cool_fan_enabled',
            'cool_fan_speed',
            'cool_fan_speed_min',
            'cool_fan_speed_max',
            'cool_min_layer_time_fan_speed_max',
            'cool_fan_speed_0',
            'cool_fan_full_at_height',
            'cool_fan_full_layer',
            'cool_min_layer_time',
            'cool_min_speed',
            'cool_lift_head'
        ]

    # format:

    # cool_fan_enabled
    # cool_fan_speed
    #     cool_fan_speed_min
    #     cool_fan_speed_max
    # cool_min_layer_time_fan_speed_max
    # cool_fan_speed_0
    # cool_fan_full_at_height
    #     cool_fan_full_layer
    # cool_min_layer_time
    # cool_min_speed
    # cool_lift_head
