# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Blackmagic(models.Model):

    print_sequence = models.CharField(
        max_length=100,
        verbose_name="Séquence d'impression",
        choices=(
                ('A', 'Tout en même temps'),
                ('O', 'Un à la fois'))
    )

    magic_mesh_surface_mode = models.CharField(
        max_length=100,
        verbose_name="Mode de surface",
        choices=(
                ("N", "Normal"),
                ("S", "Surface"),
                ("T", "Les deux"))
    )

    magic_spiralize = models.BooleanField(
        verbose_name="Spiraliser le contour extérieur",
    )

    smooth_spiralized_contours = models.BooleanField(
        verbose_name="Lisser les contours spiralisés",
    )

    class Meta():
        pass


class BlackmagicForm(ModelForm):
    class Meta:
        model = Blackmagic
        fields = [
            'print_sequence',
            'magic_mesh_surface_mode',
            'magic_spiralize',
            'smooth_spiralized_contours'
        ]

    # format:

    # print_sequence
    # magic_mesh_surface_mode
    # magic_spiralize
    # smooth_spiralized_contours
