# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm
# validator used for tcheck if values can be a percent
from .validators.validate_percent import validate_percent
# import Extruder for select extruder used in several extruder case
from printer.models.extruder import Extruder


class Infill(models.Model):

    infill_patterns = (('G', 'Grille'),
                       ('L', 'Lignes'),
                       ('T', 'Triangles'),
                       ('TH', 'Trihexagonal'),
                       ('CU', 'Cubique'),
                       ('CS', 'Subdivision cubique'),
                       ('O', 'Ocatédrique'),
                       ('QC', 'Quart cubique'),
                       ('CO', 'Concentrique'),
                       ('Z', 'Zig Zag'),
                       ('CR', 'Entrecroisé'),
                       ('CRD', 'Entrecroisé 3D'),
                       ('G', 'Gyroïde'),)

    infill_patterns_EN = (('G', 'Grid'),
                          ('L', 'Lines'),
                          ('T', 'Triangle'),
                          ('TH', 'Tri-Hexagon'),
                          ('CU', 'Cubic'),
                          ('CS', 'Cubic subdivision'),
                          ('O', 'Octet'),
                          ('QC', 'Quarter Cubic'),
                          ('CO', 'Concentric'),
                          ('Z', 'Zig Zag'),
                          ('CR', 'Cross'),
                          ('CRD', 'Cross 3D'),
                          ('G', 'Gyroid'),)

    infill_extruder_nr = models.ForeignKey(
        Extruder,
        verbose_name="Extrudeuse de remplissage",
        on_delete=models.SET_DEFAULT,
        default=1
    )

    infill_sparse_density = models.IntegerField(
        verbose_name="Densité du remplissage",
        validators=(validate_percent,),
    )

    infill_line_distance = models.FloatField(
        verbose_name="Distance d'écartement des lignes de remplissage"
    )

    infill_pattern = models.CharField(
        max_length=100,
        verbose_name="Motif de remplissage",
        choices=infill_patterns
    )

    infill_multiplier = models.IntegerField(
        verbose_name="Multiplicateur de ligne de remplissage"
    )

    infill_overlap = models.IntegerField(
        verbose_name="Pourcentage de chevauchement du remplissage",
        validators=(validate_percent,),
    )

    infill_sparse_thickness = models.IntegerField(
        verbose_name="Epaisseur de la couche de remplissage",
    )

    gradual_infill_steps = models.IntegerField(
        verbose_name="Etapes de remplissage progressif"
    )


class InfillForm(ModelForm):
    class Meta:
        model = Infill
        fields = [
            'infill_extruder_nr',
            'infill_sparse_density',
            'infill_line_distance',
            'infill_pattern',
            'infill_multiplier',
            'infill_overlap',
            'infill_sparse_thickness',
            'gradual_infill_steps'
        ]

    # format:

    # infill_extruder_nr
    # infill_sparse_density
    #     infill_line_distance
    # infill_pattern
    # infill_multiplier
    # infill_overlap
    # infill_sparse_thickness
    # gradual_infill_steps
