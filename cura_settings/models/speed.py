# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Speed(models.Model):

    speed_print = models.FloatField(
        verbose_name="Vitesse d'impression",
    )

    speed_infill = models.FloatField(
        verbose_name="Vitesse de remplissage",
    )

    speed_wall = models.FloatField(
        verbose_name="Vitesse d'impression de la paroi",
    )

    speed_wall_0 = models.FloatField(
        verbose_name="Vitesse d'impression de la paroi externe",
    )

    speed_wall_x = models.FloatField(
        verbose_name="Vitesse d'impression de la paroi interne",
    )

    speed_topbottom = models.FloatField(
        verbose_name="Vitesse d'impression du dessus/base",
    )

    speed_support = models.FloatField(
        verbose_name="Vitesse d'impression des supports",
    )

    speed_prime_tower = models.FloatField(
        verbose_name="Vitesse de la tour primaire",
    )

    speed_travel = models.FloatField(
        verbose_name="Vitesse de déplacement",
    )

    speed_layer_0 = models.FloatField(
        verbose_name="Vitesse de la couche initiale",
    )

    skirt_brim_speed = models.FloatField(
        verbose_name="Vitesse d'impression de la jupe/bordure",
    )

    acceleration_enabled = models.BooleanField(
        verbose_name="Activer le contrôle d'accélération",
    )

    jerk_enabled = models.BooleanField(
        verbose_name="Activer le controle de saccade",
    )


class SpeedForm(ModelForm):
    class Meta:
        model = Speed
        fields = [
            'speed_print',
            'speed_infill',
            'speed_wall',
            'speed_wall_0',
            'speed_wall_x',
            'speed_topbottom',
            'speed_support',
            'speed_prime_tower',
            'speed_travel',
            'speed_layer_0',
            'skirt_brim_speed',
            'acceleration_enabled',
            'jerk_enabled'
        ]

    # format:

    # speed_print
    #     speed_infill
    #     speed_wall
    #         speed_wall_0
    #         speed_wall_x
    #     speed_topbottom
    # speed_support
    #     speed_prime_tower
    # speed_travel
    # speed_layer_0
    # skirt_brim_speed
    # acceleration_enabled
    # jerk_enabled
