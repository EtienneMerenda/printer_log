# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Travel(models.Model):

    retraction_combings = (
        ('O', 'Désactivé'),
        ('A', 'Tout'),
        ('N', 'Pas dans la couche extérieure'),
        ('W', 'À l\'interieur du remplissage'),
    )

    retraction_combing = models.CharField(
        max_length=100,
        verbose_name="Mode de détours",
        choices=retraction_combings
    )

    travel_avoid_other_parts = models.BooleanField(
        verbose_name="Eviter les pièces imprimées lors du déplacement",
        default=True
    )

    travel_avoid_supports = models.BooleanField(
        verbose_name="Eviter les supports lors du déplacement"
    )

    travel_avoid_distance = models.FloatField(
        verbose_name="Distance d'évitement du déplacement"
    )

    retraction_hop_enabled = models.BooleanField(
        verbose_name="Décalage en Z lors d'un rétraction"
    )

    retraction_hop_only_when_collides = models.BooleanField(
        verbose_name="Décalage en Z uniquement sur les pièces imprimées"
    )

    retraction_hop = models.FloatField(
        verbose_name="Hauteur du décalage en Z"
    )

    retraction_hop_after_extruder_switch = models.BooleanField(
        verbose_name="Décalge en Z après changement d'extrudeuse"
    )


class TravelForm(ModelForm):
    class Meta:
        model = Travel
        fields = [
            'retraction_combing',
            'travel_avoid_other_parts',
            'travel_avoid_supports',
            'travel_avoid_distance',
            'retraction_hop_enabled',
            'retraction_hop_only_when_collides',
            'retraction_hop',
            'retraction_hop_after_extruder_switch'
        ]

    # format:

    # retraction_combing
    # travel_avoid_other_parts
    # travel_avoid_supports
    # travel_avoid_distance
    # retraction_hop_enabled
    # retraction_hop_only_when_collides
    # retraction_hop
    # retraction_hop_after_extruder_switch
