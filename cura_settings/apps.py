from django.apps import AppConfig


class PrintLogConfig(AppConfig):
    name = 'cura_settings'
