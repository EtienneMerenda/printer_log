from django.urls import re_path
from .views import settings

urlpatterns = [
    re_path('^$', settings)
]
