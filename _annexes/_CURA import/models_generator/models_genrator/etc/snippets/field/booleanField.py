{# Create booleanField with context #}
    {{context.field_name}} = models.BooleanField(
        verbose_name="{{context.label}}",
    )
