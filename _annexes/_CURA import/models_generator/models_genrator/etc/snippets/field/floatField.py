{# Create booleanField with context #}
    {{context.field_name}} = models.FloatField(
        verbose_name="{{context.label}}"
    )
