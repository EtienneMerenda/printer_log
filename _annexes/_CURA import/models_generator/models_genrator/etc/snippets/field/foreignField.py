{# Create foreignKeyField #}
    {{context.field_name}} = models.ForeignKey(
        {# if choices in keys, foreign key refer to Choices table #}
        {% if context.options %}
        Choices,
        {% elif context.type == '[int]' %}
        IntegerList,
        {% endif %}
        verbose_name='{{label}}',
        on_delete=models.SET_NULL,
    )
