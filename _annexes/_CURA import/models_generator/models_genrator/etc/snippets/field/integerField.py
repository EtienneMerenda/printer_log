{# Create booleanField with context #}
    {{context.field_name}} = models.IntegerField(
        verbose_name="{{context.label}}",
        {% if context. %}
        validators=(validate_percent,),
    )
