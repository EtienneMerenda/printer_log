{# Create charfield with context #}
    {{context.field_name}} = models.CharField(
        {# If type is list of int. Notify it #}
        {% if context.type == "[int]" %}
        # Type of data: list of int
        {% endif %}
        {# Name displayed in admin #}
        verbose_name = "{{context.label}}",
        {# Default max_length if not specified #}
        {% if max_length %}
        max_length = {{context.max_length}},
        {% else %}
        max_length=250,
        {% endif %}
        {# Add choice if need it #}
        {% if context.options %}
        choices=(
            {% for option in context.options %}
            ("{{option}}", "{{context.options[option]}}"),
            {% endfor %}
            )
        {% endif %}
    )
