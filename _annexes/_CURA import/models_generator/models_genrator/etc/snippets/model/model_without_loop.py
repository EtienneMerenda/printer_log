# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class {{ table_name }}(models.Model):


    {% with context = field %}
    {# Create charField #}
    {% if field.type == 'str' %}
    {% include "charField.py" %}
    {% elif field.type == 'enum' %}
    {% include "foreignField.py" %}
    {% elif field.type == '[int]' %}
    {% include "foreignField.py" %}
    {# Create booleanField #}
    {% elif field.type == 'bool' %}
    {% include "booleanField.py" %}
    {# Create floatField #}
    {% elif field.type == 'float' %}
    {% include "floatField.py" %}
    {# Create intField #}
    {% elif field.type == 'int' %}
    {% include "floatField.py" %}
    {# Create foreignkey for extruder #}
    {% else %}
    {{ field.type }}
    {% endif %}
    {% endwith %}


class {{ table_name }}Form(ModelForm):
    class Meta:
        model = {{ table_name }}
        fields = [
            '{{ field.field_name }}'
        ]
