# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Platform_Adhesion(models.Model):

    prime_blob_enable = models.BooleanField(
        verbose_name="Enable Prime Blob",
    )

    extruder_prime_pos_x = models.FloatField(
        verbose_name="Extruder Prime X Position"
    )

    extruder_prime_pos_y = models.FloatField(
        verbose_name="Extruder Prime Y Position"
    )

    adhesion_type = models.ForeignKey(
        Choices,
        verbose_name='platform_adhesion',
        on_delete=models.SET_NULL,
    )

    extruder


    skirt_line_count = models.FloatField(
        verbose_name="Skirt Line Count"
    )

    skirt_gap = models.FloatField(
        verbose_name="Skirt Distance"
    )

    skirt_brim_minimal_length = models.FloatField(
        verbose_name="Skirt/Brim Minimum Length"
    )

    brim_width = models.FloatField(
        verbose_name="Brim Width"
    )

    brim_line_count = models.FloatField(
        verbose_name="Brim Line Count"
    )

    brim_replaces_support = models.BooleanField(
        verbose_name="Brim Replaces Support",
    )

    brim_outside_only = models.BooleanField(
        verbose_name="Brim Only on Outside",
    )

    raft_margin = models.FloatField(
        verbose_name="Raft Extra Margin"
    )

    raft_smoothing = models.FloatField(
        verbose_name="Raft Smoothing"
    )

    raft_airgap = models.FloatField(
        verbose_name="Raft Air Gap"
    )

    layer_0_z_overlap = models.FloatField(
        verbose_name="Initial Layer Z Overlap"
    )

    raft_surface_layers = models.FloatField(
        verbose_name="Raft Top Layers"
    )

    raft_surface_thickness = models.FloatField(
        verbose_name="Raft Top Layer Thickness"
    )

    raft_surface_line_width = models.FloatField(
        verbose_name="Raft Top Line Width"
    )

    raft_surface_line_spacing = models.FloatField(
        verbose_name="Raft Top Spacing"
    )

    raft_interface_thickness = models.FloatField(
        verbose_name="Raft Middle Thickness"
    )

    raft_interface_line_width = models.FloatField(
        verbose_name="Raft Middle Line Width"
    )

    raft_interface_line_spacing = models.FloatField(
        verbose_name="Raft Middle Spacing"
    )

    raft_base_thickness = models.FloatField(
        verbose_name="Raft Base Thickness"
    )

    raft_base_line_width = models.FloatField(
        verbose_name="Raft Base Line Width"
    )

    raft_base_line_spacing = models.FloatField(
        verbose_name="Raft Base Line Spacing"
    )

    raft_speed = models.FloatField(
        verbose_name="Raft Print Speed"
    )

    raft_surface_speed = models.FloatField(
        verbose_name="Raft Top Print Speed"
    )

    raft_interface_speed = models.FloatField(
        verbose_name="Raft Middle Print Speed"
    )

    raft_base_speed = models.FloatField(
        verbose_name="Raft Base Print Speed"
    )

    raft_acceleration = models.FloatField(
        verbose_name="Raft Print Acceleration"
    )

    raft_surface_acceleration = models.FloatField(
        verbose_name="Raft Top Print Acceleration"
    )

    raft_interface_acceleration = models.FloatField(
        verbose_name="Raft Middle Print Acceleration"
    )

    raft_base_acceleration = models.FloatField(
        verbose_name="Raft Base Print Acceleration"
    )

    raft_jerk = models.FloatField(
        verbose_name="Raft Print Jerk"
    )

    raft_surface_jerk = models.FloatField(
        verbose_name="Raft Top Print Jerk"
    )

    raft_interface_jerk = models.FloatField(
        verbose_name="Raft Middle Print Jerk"
    )

    raft_base_jerk = models.FloatField(
        verbose_name="Raft Base Print Jerk"
    )

    raft_fan_speed = models.FloatField(
        verbose_name="Raft Fan Speed"
    )

    raft_surface_fan_speed = models.FloatField(
        verbose_name="Raft Top Fan Speed"
    )

    raft_interface_fan_speed = models.FloatField(
        verbose_name="Raft Middle Fan Speed"
    )

    raft_base_fan_speed = models.FloatField(
        verbose_name="Raft Base Fan Speed"
    )


class Platform_AdhesionForm(ModelForm):
    class Meta:
        model = Platform_Adhesion
        fields = [
            'prime_blob_enable',
            'extruder_prime_pos_x',
            'extruder_prime_pos_y',
            'adhesion_type',
            'adhesion_extruder_nr',
            'skirt_line_count',
            'skirt_gap',
            'skirt_brim_minimal_length',
            'brim_width',
            'brim_line_count',
            'brim_replaces_support',
            'brim_outside_only',
            'raft_margin',
            'raft_smoothing',
            'raft_airgap',
            'layer_0_z_overlap',
            'raft_surface_layers',
            'raft_surface_thickness',
            'raft_surface_line_width',
            'raft_surface_line_spacing',
            'raft_interface_thickness',
            'raft_interface_line_width',
            'raft_interface_line_spacing',
            'raft_base_thickness',
            'raft_base_line_width',
            'raft_base_line_spacing',
            'raft_speed',
            'raft_surface_speed',
            'raft_interface_speed',
            'raft_base_speed',
            'raft_acceleration',
            'raft_surface_acceleration',
            'raft_interface_acceleration',
            'raft_base_acceleration',
            'raft_jerk',
            'raft_surface_jerk',
            'raft_interface_jerk',
            'raft_base_jerk',
            'raft_fan_speed',
            'raft_surface_fan_speed',
            'raft_interface_fan_speed',
            'raft_base_fan_speed',
        ]