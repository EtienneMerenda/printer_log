# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Speed(models.Model):

    speed_print = models.FloatField(
        verbose_name="Print Speed"
    )

    speed_infill = models.FloatField(
        verbose_name="Infill Speed"
    )

    speed_wall = models.FloatField(
        verbose_name="Wall Speed"
    )

    speed_wall_0 = models.FloatField(
        verbose_name="Outer Wall Speed"
    )

    speed_wall_x = models.FloatField(
        verbose_name="Inner Wall Speed"
    )

    speed_roofing = models.FloatField(
        verbose_name="Top Surface Skin Speed"
    )

    speed_topbottom = models.FloatField(
        verbose_name="Top/Bottom Speed"
    )

    speed_support = models.FloatField(
        verbose_name="Support Speed"
    )

    speed_support_infill = models.FloatField(
        verbose_name="Support Infill Speed"
    )

    speed_support_interface = models.FloatField(
        verbose_name="Support Interface Speed"
    )

    speed_support_roof = models.FloatField(
        verbose_name="Support Roof Speed"
    )

    speed_support_bottom = models.FloatField(
        verbose_name="Support Floor Speed"
    )

    speed_prime_tower = models.FloatField(
        verbose_name="Prime Tower Speed"
    )

    speed_travel = models.FloatField(
        verbose_name="Travel Speed"
    )

    speed_layer_0 = models.FloatField(
        verbose_name="Initial Layer Speed"
    )

    speed_print_layer_0 = models.FloatField(
        verbose_name="Initial Layer Print Speed"
    )

    speed_travel_layer_0 = models.FloatField(
        verbose_name="Initial Layer Travel Speed"
    )

    skirt_brim_speed = models.FloatField(
        verbose_name="Skirt/Brim Speed"
    )

    speed_z_hop = models.FloatField(
        verbose_name="Z Hop Speed"
    )

    speed_slowdown_layers = models.FloatField(
        verbose_name="Number of Slower Layers"
    )

    speed_equalize_flow_enabled = models.BooleanField(
        verbose_name="Equalize Filament Flow",
    )

    speed_equalize_flow_max = models.FloatField(
        verbose_name="Maximum Speed for Flow Equalization"
    )

    acceleration_enabled = models.BooleanField(
        verbose_name="Enable Acceleration Control",
    )

    acceleration_print = models.FloatField(
        verbose_name="Print Acceleration"
    )

    acceleration_infill = models.FloatField(
        verbose_name="Infill Acceleration"
    )

    acceleration_wall = models.FloatField(
        verbose_name="Wall Acceleration"
    )

    acceleration_wall_0 = models.FloatField(
        verbose_name="Outer Wall Acceleration"
    )

    acceleration_wall_x = models.FloatField(
        verbose_name="Inner Wall Acceleration"
    )

    acceleration_roofing = models.FloatField(
        verbose_name="Top Surface Skin Acceleration"
    )

    acceleration_topbottom = models.FloatField(
        verbose_name="Top/Bottom Acceleration"
    )

    acceleration_support = models.FloatField(
        verbose_name="Support Acceleration"
    )

    acceleration_support_infill = models.FloatField(
        verbose_name="Support Infill Acceleration"
    )

    acceleration_support_interface = models.FloatField(
        verbose_name="Support Interface Acceleration"
    )

    acceleration_support_roof = models.FloatField(
        verbose_name="Support Roof Acceleration"
    )

    acceleration_support_bottom = models.FloatField(
        verbose_name="Support Floor Acceleration"
    )

    acceleration_prime_tower = models.FloatField(
        verbose_name="Prime Tower Acceleration"
    )

    acceleration_travel = models.FloatField(
        verbose_name="Travel Acceleration"
    )

    acceleration_layer_0 = models.FloatField(
        verbose_name="Initial Layer Acceleration"
    )

    acceleration_print_layer_0 = models.FloatField(
        verbose_name="Initial Layer Print Acceleration"
    )

    acceleration_travel_layer_0 = models.FloatField(
        verbose_name="Initial Layer Travel Acceleration"
    )

    acceleration_skirt_brim = models.FloatField(
        verbose_name="Skirt/Brim Acceleration"
    )

    jerk_enabled = models.BooleanField(
        verbose_name="Enable Jerk Control",
    )

    jerk_print = models.FloatField(
        verbose_name="Print Jerk"
    )

    jerk_infill = models.FloatField(
        verbose_name="Infill Jerk"
    )

    jerk_wall = models.FloatField(
        verbose_name="Wall Jerk"
    )

    jerk_wall_0 = models.FloatField(
        verbose_name="Outer Wall Jerk"
    )

    jerk_wall_x = models.FloatField(
        verbose_name="Inner Wall Jerk"
    )

    jerk_roofing = models.FloatField(
        verbose_name="Top Surface Skin Jerk"
    )

    jerk_topbottom = models.FloatField(
        verbose_name="Top/Bottom Jerk"
    )

    jerk_support = models.FloatField(
        verbose_name="Support Jerk"
    )

    jerk_support_infill = models.FloatField(
        verbose_name="Support Infill Jerk"
    )

    jerk_support_interface = models.FloatField(
        verbose_name="Support Interface Jerk"
    )

    jerk_support_roof = models.FloatField(
        verbose_name="Support Roof Jerk"
    )

    jerk_support_bottom = models.FloatField(
        verbose_name="Support Floor Jerk"
    )

    jerk_prime_tower = models.FloatField(
        verbose_name="Prime Tower Jerk"
    )

    jerk_travel = models.FloatField(
        verbose_name="Travel Jerk"
    )

    jerk_layer_0 = models.FloatField(
        verbose_name="Initial Layer Jerk"
    )

    jerk_print_layer_0 = models.FloatField(
        verbose_name="Initial Layer Print Jerk"
    )

    jerk_travel_layer_0 = models.FloatField(
        verbose_name="Initial Layer Travel Jerk"
    )

    jerk_skirt_brim = models.FloatField(
        verbose_name="Skirt/Brim Jerk"
    )


class SpeedForm(ModelForm):
    class Meta:
        model = Speed
        fields = [
            'speed_print',
            'speed_infill',
            'speed_wall',
            'speed_wall_0',
            'speed_wall_x',
            'speed_roofing',
            'speed_topbottom',
            'speed_support',
            'speed_support_infill',
            'speed_support_interface',
            'speed_support_roof',
            'speed_support_bottom',
            'speed_prime_tower',
            'speed_travel',
            'speed_layer_0',
            'speed_print_layer_0',
            'speed_travel_layer_0',
            'skirt_brim_speed',
            'speed_z_hop',
            'speed_slowdown_layers',
            'speed_equalize_flow_enabled',
            'speed_equalize_flow_max',
            'acceleration_enabled',
            'acceleration_print',
            'acceleration_infill',
            'acceleration_wall',
            'acceleration_wall_0',
            'acceleration_wall_x',
            'acceleration_roofing',
            'acceleration_topbottom',
            'acceleration_support',
            'acceleration_support_infill',
            'acceleration_support_interface',
            'acceleration_support_roof',
            'acceleration_support_bottom',
            'acceleration_prime_tower',
            'acceleration_travel',
            'acceleration_layer_0',
            'acceleration_print_layer_0',
            'acceleration_travel_layer_0',
            'acceleration_skirt_brim',
            'jerk_enabled',
            'jerk_print',
            'jerk_infill',
            'jerk_wall',
            'jerk_wall_0',
            'jerk_wall_x',
            'jerk_roofing',
            'jerk_topbottom',
            'jerk_support',
            'jerk_support_infill',
            'jerk_support_interface',
            'jerk_support_roof',
            'jerk_support_bottom',
            'jerk_prime_tower',
            'jerk_travel',
            'jerk_layer_0',
            'jerk_print_layer_0',
            'jerk_travel_layer_0',
            'jerk_skirt_brim',
        ]