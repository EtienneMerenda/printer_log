# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Travel(models.Model):

    retraction_combing = models.ForeignKey(
        Choices,
        verbose_name='travel',
        on_delete=models.SET_NULL,
    )

    retraction_combing_max_distance = models.FloatField(
        verbose_name="Max Comb Distance With No Retract"
    )

    travel_retract_before_outer_wall = models.BooleanField(
        verbose_name="Retract Before Outer Wall",
    )

    travel_avoid_other_parts = models.BooleanField(
        verbose_name="Avoid Printed Parts When Traveling",
    )

    travel_avoid_supports = models.BooleanField(
        verbose_name="Avoid Supports When Traveling",
    )

    travel_avoid_distance = models.FloatField(
        verbose_name="Travel Avoid Distance"
    )

    start_layers_at_same_position = models.BooleanField(
        verbose_name="Start Layers with the Same Part",
    )

    layer_start_x = models.FloatField(
        verbose_name="Layer Start X"
    )

    layer_start_y = models.FloatField(
        verbose_name="Layer Start Y"
    )

    retraction_hop_enabled = models.BooleanField(
        verbose_name="Z Hop When Retracted",
    )

    retraction_hop_only_when_collides = models.BooleanField(
        verbose_name="Z Hop Only Over Printed Parts",
    )

    retraction_hop = models.FloatField(
        verbose_name="Z Hop Height"
    )

    retraction_hop_after_extruder_switch = models.BooleanField(
        verbose_name="Z Hop After Extruder Switch",
    )

    retraction_hop_after_extruder_switch_height = models.FloatField(
        verbose_name="Z Hop After Extruder Switch Height"
    )


class TravelForm(ModelForm):
    class Meta:
        model = Travel
        fields = [
            'retraction_combing',
            'retraction_combing_max_distance',
            'travel_retract_before_outer_wall',
            'travel_avoid_other_parts',
            'travel_avoid_supports',
            'travel_avoid_distance',
            'start_layers_at_same_position',
            'layer_start_x',
            'layer_start_y',
            'retraction_hop_enabled',
            'retraction_hop_only_when_collides',
            'retraction_hop',
            'retraction_hop_after_extruder_switch',
            'retraction_hop_after_extruder_switch_height',
        ]