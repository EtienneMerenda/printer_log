# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Shell(models.Model):

    optional_extruder


    optional_extruder


    optional_extruder


    wall_thickness = models.FloatField(
        verbose_name="Wall Thickness"
    )

    wall_line_count = models.FloatField(
        verbose_name="Wall Line Count"
    )

    wall_0_wipe_dist = models.FloatField(
        verbose_name="Outer Wall Wipe Distance"
    )

    optional_extruder


    roofing_layer_count = models.FloatField(
        verbose_name="Top Surface Skin Layers"
    )

    optional_extruder


    top_bottom_thickness = models.FloatField(
        verbose_name="Top/Bottom Thickness"
    )

    top_thickness = models.FloatField(
        verbose_name="Top Thickness"
    )

    top_layers = models.FloatField(
        verbose_name="Top Layers"
    )

    bottom_thickness = models.FloatField(
        verbose_name="Bottom Thickness"
    )

    bottom_layers = models.FloatField(
        verbose_name="Bottom Layers"
    )

    top_bottom_pattern = models.ForeignKey(
        Choices,
        verbose_name='shell',
        on_delete=models.SET_NULL,
    )

    top_bottom_pattern_0 = models.ForeignKey(
        Choices,
        verbose_name='shell',
        on_delete=models.SET_NULL,
    )

    connect_skin_polygons = models.BooleanField(
        verbose_name="Connect Top/Bottom Polygons",
    )

    skin_angles = models.ForeignKey(
        IntegerList,
        verbose_name='shell',
        on_delete=models.SET_NULL,
    )

    wall_0_inset = models.FloatField(
        verbose_name="Outer Wall Inset"
    )

    optimize_wall_printing_order = models.BooleanField(
        verbose_name="Optimize Wall Printing Order",
    )

    outer_inset_first = models.BooleanField(
        verbose_name="Outer Before Inner Walls",
    )

    alternate_extra_perimeter = models.BooleanField(
        verbose_name="Alternate Extra Wall",
    )

    travel_compensate_overlapping_walls_enabled = models.BooleanField(
        verbose_name="Compensate Wall Overlaps",
    )

    travel_compensate_overlapping_walls_0_enabled = models.BooleanField(
        verbose_name="Compensate Outer Wall Overlaps",
    )

    travel_compensate_overlapping_walls_x_enabled = models.BooleanField(
        verbose_name="Compensate Inner Wall Overlaps",
    )

    wall_min_flow = models.FloatField(
        verbose_name="Minimum Wall Flow"
    )

    wall_min_flow_retract = models.BooleanField(
        verbose_name="Prefer Retract",
    )

    fill_perimeter_gaps = models.ForeignKey(
        Choices,
        verbose_name='shell',
        on_delete=models.SET_NULL,
    )

    filter_out_tiny_gaps = models.BooleanField(
        verbose_name="Filter Out Tiny Gaps",
    )

    fill_outline_gaps = models.BooleanField(
        verbose_name="Print Thin Walls",
    )

    xy_offset = models.FloatField(
        verbose_name="Horizontal Expansion"
    )

    xy_offset_layer_0 = models.FloatField(
        verbose_name="Initial Layer Horizontal Expansion"
    )

    z_seam_type = models.ForeignKey(
        Choices,
        verbose_name='shell',
        on_delete=models.SET_NULL,
    )

    z_seam_x = models.FloatField(
        verbose_name="Z Seam X"
    )

    z_seam_y = models.FloatField(
        verbose_name="Z Seam Y"
    )

    z_seam_corner = models.ForeignKey(
        Choices,
        verbose_name='shell',
        on_delete=models.SET_NULL,
    )

    z_seam_relative = models.BooleanField(
        verbose_name="Z Seam Relative",
    )

    skin_no_small_gaps_heuristic = models.BooleanField(
        verbose_name="No Skin in Z Gaps",
    )

    skin_outline_count = models.FloatField(
        verbose_name="Extra Skin Wall Count"
    )

    ironing_enabled = models.BooleanField(
        verbose_name="Enable Ironing",
    )

    ironing_only_highest_layer = models.BooleanField(
        verbose_name="Iron Only Highest Layer",
    )

    ironing_pattern = models.ForeignKey(
        Choices,
        verbose_name='shell',
        on_delete=models.SET_NULL,
    )

    ironing_line_spacing = models.FloatField(
        verbose_name="Ironing Line Spacing"
    )

    ironing_flow = models.FloatField(
        verbose_name="Ironing Flow"
    )

    ironing_inset = models.FloatField(
        verbose_name="Ironing Inset"
    )

    speed_ironing = models.FloatField(
        verbose_name="Ironing Speed"
    )

    acceleration_ironing = models.FloatField(
        verbose_name="Ironing Acceleration"
    )

    jerk_ironing = models.FloatField(
        verbose_name="Ironing Jerk"
    )


class ShellForm(ModelForm):
    class Meta:
        model = Shell
        fields = [
            'wall_extruder_nr',
            'wall_0_extruder_nr',
            'wall_x_extruder_nr',
            'wall_thickness',
            'wall_line_count',
            'wall_0_wipe_dist',
            'roofing_extruder_nr',
            'roofing_layer_count',
            'top_bottom_extruder_nr',
            'top_bottom_thickness',
            'top_thickness',
            'top_layers',
            'bottom_thickness',
            'bottom_layers',
            'top_bottom_pattern',
            'top_bottom_pattern_0',
            'connect_skin_polygons',
            'skin_angles',
            'wall_0_inset',
            'optimize_wall_printing_order',
            'outer_inset_first',
            'alternate_extra_perimeter',
            'travel_compensate_overlapping_walls_enabled',
            'travel_compensate_overlapping_walls_0_enabled',
            'travel_compensate_overlapping_walls_x_enabled',
            'wall_min_flow',
            'wall_min_flow_retract',
            'fill_perimeter_gaps',
            'filter_out_tiny_gaps',
            'fill_outline_gaps',
            'xy_offset',
            'xy_offset_layer_0',
            'z_seam_type',
            'z_seam_x',
            'z_seam_y',
            'z_seam_corner',
            'z_seam_relative',
            'skin_no_small_gaps_heuristic',
            'skin_outline_count',
            'ironing_enabled',
            'ironing_only_highest_layer',
            'ironing_pattern',
            'ironing_line_spacing',
            'ironing_flow',
            'ironing_inset',
            'speed_ironing',
            'acceleration_ironing',
            'jerk_ironing',
        ]