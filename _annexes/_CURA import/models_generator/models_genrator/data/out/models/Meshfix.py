# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Meshfix(models.Model):

    meshfix_union_all = models.BooleanField(
        verbose_name="Union Overlapping Volumes",
    )

    meshfix_union_all_remove_holes = models.BooleanField(
        verbose_name="Remove All Holes",
    )

    meshfix_extensive_stitching = models.BooleanField(
        verbose_name="Extensive Stitching",
    )

    meshfix_keep_open_polygons = models.BooleanField(
        verbose_name="Keep Disconnected Faces",
    )

    multiple_mesh_overlap = models.FloatField(
        verbose_name="Merged Meshes Overlap"
    )

    carve_multiple_volumes = models.BooleanField(
        verbose_name="Remove Mesh Intersection",
    )

    alternate_carve_order = models.BooleanField(
        verbose_name="Alternate Mesh Removal",
    )

    remove_empty_first_layers = models.BooleanField(
        verbose_name="Remove Empty First Layers",
    )


class MeshfixForm(ModelForm):
    class Meta:
        model = Meshfix
        fields = [
            'meshfix_union_all',
            'meshfix_union_all_remove_holes',
            'meshfix_extensive_stitching',
            'meshfix_keep_open_polygons',
            'multiple_mesh_overlap',
            'carve_multiple_volumes',
            'alternate_carve_order',
            'remove_empty_first_layers',
        ]