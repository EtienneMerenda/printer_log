# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Cooling(models.Model):

    cool_fan_enabled = models.BooleanField(
        verbose_name="Enable Print Cooling",
    )

    cool_fan_speed = models.FloatField(
        verbose_name="Fan Speed"
    )

    cool_fan_speed_min = models.FloatField(
        verbose_name="Regular Fan Speed"
    )

    cool_fan_speed_max = models.FloatField(
        verbose_name="Maximum Fan Speed"
    )

    cool_min_layer_time_fan_speed_max = models.FloatField(
        verbose_name="Regular/Maximum Fan Speed Threshold"
    )

    cool_fan_speed_0 = models.FloatField(
        verbose_name="Initial Fan Speed"
    )

    cool_fan_full_at_height = models.FloatField(
        verbose_name="Regular Fan Speed at Height"
    )

    cool_fan_full_layer = models.FloatField(
        verbose_name="Regular Fan Speed at Layer"
    )

    cool_min_layer_time = models.FloatField(
        verbose_name="Minimum Layer Time"
    )

    cool_min_speed = models.FloatField(
        verbose_name="Minimum Speed"
    )

    cool_lift_head = models.BooleanField(
        verbose_name="Lift Head",
    )


class CoolingForm(ModelForm):
    class Meta:
        model = Cooling
        fields = [
            'cool_fan_enabled',
            'cool_fan_speed',
            'cool_fan_speed_min',
            'cool_fan_speed_max',
            'cool_min_layer_time_fan_speed_max',
            'cool_fan_speed_0',
            'cool_fan_full_at_height',
            'cool_fan_full_layer',
            'cool_min_layer_time',
            'cool_min_speed',
            'cool_lift_head',
        ]