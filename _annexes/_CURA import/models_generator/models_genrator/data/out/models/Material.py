# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Material(models.Model):

    default_material_print_temperature = models.FloatField(
        verbose_name="Default Printing Temperature"
    )

    build_volume_temperature = models.FloatField(
        verbose_name="Build Volume Temperature"
    )

    material_print_temperature = models.FloatField(
        verbose_name="Printing Temperature"
    )

    material_print_temperature_layer_0 = models.FloatField(
        verbose_name="Printing Temperature Initial Layer"
    )

    material_initial_print_temperature = models.FloatField(
        verbose_name="Initial Printing Temperature"
    )

    material_final_print_temperature = models.FloatField(
        verbose_name="Final Printing Temperature"
    )

    material_extrusion_cool_down_speed = models.FloatField(
        verbose_name="Extrusion Cool Down Speed Modifier"
    )

    default_material_bed_temperature = models.FloatField(
        verbose_name="Default Build Plate Temperature"
    )

    material_bed_temperature = models.FloatField(
        verbose_name="Build Plate Temperature"
    )

    material_bed_temperature_layer_0 = models.FloatField(
        verbose_name="Build Plate Temperature Initial Layer"
    )

    material_adhesion_tendency = models.FloatField(
        verbose_name="Adhesion Tendency"
    )

    material_surface_energy = models.FloatField(
        verbose_name="Surface Energy"
    )

    material_shrinkage_percentage = models.FloatField(
        verbose_name="Shrinkage Ratio"
    )

    material_crystallinity = models.BooleanField(
        verbose_name="Crystalline Material",
    )

    material_anti_ooze_retracted_position = models.FloatField(
        verbose_name="Anti-ooze Retracted Position"
    )

    material_anti_ooze_retraction_speed = models.FloatField(
        verbose_name="Anti-ooze Retraction Speed"
    )

    material_break_preparation_retracted_position = models.FloatField(
        verbose_name="Break Preparation Retracted Position"
    )

    material_break_preparation_speed = models.FloatField(
        verbose_name="Break Preparation Retraction Speed"
    )

    material_break_retracted_position = models.FloatField(
        verbose_name="Break Retracted Position"
    )

    material_break_speed = models.FloatField(
        verbose_name="Break Retraction Speed"
    )

    material_break_temperature = models.FloatField(
        verbose_name="Break Temperature"
    )

    material_flow = models.FloatField(
        verbose_name="Flow"
    )

    wall_material_flow = models.FloatField(
        verbose_name="Wall Flow"
    )

    wall_0_material_flow = models.FloatField(
        verbose_name="Outer Wall Flow"
    )

    wall_x_material_flow = models.FloatField(
        verbose_name="Inner Wall(s) Flow"
    )

    skin_material_flow = models.FloatField(
        verbose_name="Top/Bottom Flow"
    )

    roofing_material_flow = models.FloatField(
        verbose_name="Top Surface Skin Flow"
    )

    infill_material_flow = models.FloatField(
        verbose_name="Infill Flow"
    )

    skirt_brim_material_flow = models.FloatField(
        verbose_name="Skirt/Brim Flow"
    )

    support_material_flow = models.FloatField(
        verbose_name="Support Flow"
    )

    support_interface_material_flow = models.FloatField(
        verbose_name="Support Interface Flow"
    )

    support_roof_material_flow = models.FloatField(
        verbose_name="Support Roof Flow"
    )

    support_bottom_material_flow = models.FloatField(
        verbose_name="Support Floor Flow"
    )

    prime_tower_flow = models.FloatField(
        verbose_name="Prime Tower Flow"
    )

    material_flow_layer_0 = models.FloatField(
        verbose_name="Initial Layer Flow"
    )

    retraction_enable = models.BooleanField(
        verbose_name="Enable Retraction",
    )

    retract_at_layer_change = models.BooleanField(
        verbose_name="Retract at Layer Change",
    )

    retraction_amount = models.FloatField(
        verbose_name="Retraction Distance"
    )

    retraction_speed = models.FloatField(
        verbose_name="Retraction Speed"
    )

    retraction_retract_speed = models.FloatField(
        verbose_name="Retraction Retract Speed"
    )

    retraction_prime_speed = models.FloatField(
        verbose_name="Retraction Prime Speed"
    )

    retraction_extra_prime_amount = models.FloatField(
        verbose_name="Retraction Extra Prime Amount"
    )

    retraction_min_travel = models.FloatField(
        verbose_name="Retraction Minimum Travel"
    )

    retraction_count_max = models.FloatField(
        verbose_name="Maximum Retraction Count"
    )

    retraction_extrusion_window = models.FloatField(
        verbose_name="Minimum Extrusion Distance Window"
    )

    limit_support_retractions = models.BooleanField(
        verbose_name="Limit Support Retractions",
    )

    material_standby_temperature = models.FloatField(
        verbose_name="Standby Temperature"
    )

    switch_extruder_retraction_amount = models.FloatField(
        verbose_name="Nozzle Switch Retraction Distance"
    )

    switch_extruder_retraction_speeds = models.FloatField(
        verbose_name="Nozzle Switch Retraction Speed"
    )

    switch_extruder_retraction_speed = models.FloatField(
        verbose_name="Nozzle Switch Retract Speed"
    )

    switch_extruder_prime_speed = models.FloatField(
        verbose_name="Nozzle Switch Prime Speed"
    )

    switch_extruder_extra_prime_amount = models.FloatField(
        verbose_name="Nozzle Switch Extra Prime Amount"
    )


class MaterialForm(ModelForm):
    class Meta:
        model = Material
        fields = [
            'default_material_print_temperature',
            'build_volume_temperature',
            'material_print_temperature',
            'material_print_temperature_layer_0',
            'material_initial_print_temperature',
            'material_final_print_temperature',
            'material_extrusion_cool_down_speed',
            'default_material_bed_temperature',
            'material_bed_temperature',
            'material_bed_temperature_layer_0',
            'material_adhesion_tendency',
            'material_surface_energy',
            'material_shrinkage_percentage',
            'material_crystallinity',
            'material_anti_ooze_retracted_position',
            'material_anti_ooze_retraction_speed',
            'material_break_preparation_retracted_position',
            'material_break_preparation_speed',
            'material_break_retracted_position',
            'material_break_speed',
            'material_break_temperature',
            'material_flow',
            'wall_material_flow',
            'wall_0_material_flow',
            'wall_x_material_flow',
            'skin_material_flow',
            'roofing_material_flow',
            'infill_material_flow',
            'skirt_brim_material_flow',
            'support_material_flow',
            'support_interface_material_flow',
            'support_roof_material_flow',
            'support_bottom_material_flow',
            'prime_tower_flow',
            'material_flow_layer_0',
            'retraction_enable',
            'retract_at_layer_change',
            'retraction_amount',
            'retraction_speed',
            'retraction_retract_speed',
            'retraction_prime_speed',
            'retraction_extra_prime_amount',
            'retraction_min_travel',
            'retraction_count_max',
            'retraction_extrusion_window',
            'limit_support_retractions',
            'material_standby_temperature',
            'switch_extruder_retraction_amount',
            'switch_extruder_retraction_speeds',
            'switch_extruder_retraction_speed',
            'switch_extruder_prime_speed',
            'switch_extruder_extra_prime_amount',
        ]