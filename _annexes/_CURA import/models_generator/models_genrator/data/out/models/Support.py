# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Support(models.Model):

    support_enable = models.BooleanField(
        verbose_name="Generate Support",
    )

    extruder


    extruder


    extruder


    extruder


    extruder


    extruder


    support_type = models.ForeignKey(
        Choices,
        verbose_name='support',
        on_delete=models.SET_NULL,
    )

    support_angle = models.FloatField(
        verbose_name="Support Overhang Angle"
    )

    support_pattern = models.ForeignKey(
        Choices,
        verbose_name='support',
        on_delete=models.SET_NULL,
    )

    support_wall_count = models.FloatField(
        verbose_name="Support Wall Line Count"
    )

    zig_zaggify_support = models.BooleanField(
        verbose_name="Connect Support Lines",
    )

    support_connect_zigzags = models.BooleanField(
        verbose_name="Connect Support ZigZags",
    )

    support_infill_rate = models.FloatField(
        verbose_name="Support Density"
    )

    support_line_distance = models.FloatField(
        verbose_name="Support Line Distance"
    )

    support_initial_layer_line_distance = models.FloatField(
        verbose_name="Initial Layer Support Line Distance"
    )

    support_infill_angle = models.FloatField(
        verbose_name="Support Infill Line Direction"
    )

    support_brim_enable = models.BooleanField(
        verbose_name="Enable Support Brim",
    )

    support_brim_width = models.FloatField(
        verbose_name="Support Brim Width"
    )

    support_brim_line_count = models.FloatField(
        verbose_name="Support Brim Line Count"
    )

    support_z_distance = models.FloatField(
        verbose_name="Support Z Distance"
    )

    support_top_distance = models.FloatField(
        verbose_name="Support Top Distance"
    )

    support_bottom_distance = models.FloatField(
        verbose_name="Support Bottom Distance"
    )

    support_xy_distance = models.FloatField(
        verbose_name="Support X/Y Distance"
    )

    support_xy_overrides_z = models.ForeignKey(
        Choices,
        verbose_name='support',
        on_delete=models.SET_NULL,
    )

    support_xy_distance_overhang = models.FloatField(
        verbose_name="Minimum Support X/Y Distance"
    )

    support_bottom_stair_step_height = models.FloatField(
        verbose_name="Support Stair Step Height"
    )

    support_bottom_stair_step_width = models.FloatField(
        verbose_name="Support Stair Step Maximum Width"
    )

    support_join_distance = models.FloatField(
        verbose_name="Support Join Distance"
    )

    support_offset = models.FloatField(
        verbose_name="Support Horizontal Expansion"
    )

    support_infill_sparse_thickness = models.FloatField(
        verbose_name="Support Infill Layer Thickness"
    )

    gradual_support_infill_steps = models.FloatField(
        verbose_name="Gradual Support Infill Steps"
    )

    gradual_support_infill_step_height = models.FloatField(
        verbose_name="Gradual Support Infill Step Height"
    )

    minimum_support_area = models.FloatField(
        verbose_name="Minimum Support Area"
    )

    support_interface_enable = models.BooleanField(
        verbose_name="Enable Support Interface",
    )

    support_roof_enable = models.BooleanField(
        verbose_name="Enable Support Roof",
    )

    support_bottom_enable = models.BooleanField(
        verbose_name="Enable Support Floor",
    )

    support_interface_height = models.FloatField(
        verbose_name="Support Interface Thickness"
    )

    support_roof_height = models.FloatField(
        verbose_name="Support Roof Thickness"
    )

    support_bottom_height = models.FloatField(
        verbose_name="Support Floor Thickness"
    )

    support_interface_skip_height = models.FloatField(
        verbose_name="Support Interface Resolution"
    )

    support_interface_density = models.FloatField(
        verbose_name="Support Interface Density"
    )

    support_roof_density = models.FloatField(
        verbose_name="Support Roof Density"
    )

    support_roof_line_distance = models.FloatField(
        verbose_name="Support Roof Line Distance"
    )

    support_bottom_density = models.FloatField(
        verbose_name="Support Floor Density"
    )

    support_bottom_line_distance = models.FloatField(
        verbose_name="Support Floor Line Distance"
    )

    support_interface_pattern = models.ForeignKey(
        Choices,
        verbose_name='support',
        on_delete=models.SET_NULL,
    )

    support_roof_pattern = models.ForeignKey(
        Choices,
        verbose_name='support',
        on_delete=models.SET_NULL,
    )

    support_bottom_pattern = models.ForeignKey(
        Choices,
        verbose_name='support',
        on_delete=models.SET_NULL,
    )

    minimum_interface_area = models.FloatField(
        verbose_name="Minimum Support Interface Area"
    )

    minimum_roof_area = models.FloatField(
        verbose_name="Minimum Support Roof Area"
    )

    minimum_bottom_area = models.FloatField(
        verbose_name="Minimum Support Floor Area"
    )

    support_interface_offset = models.FloatField(
        verbose_name="Support Interface Horizontal Expansion"
    )

    support_roof_offset = models.FloatField(
        verbose_name="Support Roof Horizontal Expansion"
    )

    support_bottom_offset = models.FloatField(
        verbose_name="Support Floor Horizontal Expansion"
    )

    support_fan_enable = models.BooleanField(
        verbose_name="Fan Speed Override",
    )

    support_supported_skin_fan_speed = models.FloatField(
        verbose_name="Supported Skin Fan Speed"
    )

    support_use_towers = models.BooleanField(
        verbose_name="Use Towers",
    )

    support_tower_diameter = models.FloatField(
        verbose_name="Tower Diameter"
    )

    support_tower_maximum_supported_diameter = models.FloatField(
        verbose_name="Maximum Tower-Supported Diameter"
    )

    support_tower_roof_angle = models.FloatField(
        verbose_name="Tower Roof Angle"
    )

    support_mesh_drop_down = models.BooleanField(
        verbose_name="Drop Down Support Mesh",
    )


class SupportForm(ModelForm):
    class Meta:
        model = Support
        fields = [
            'support_enable',
            'support_extruder_nr',
            'support_infill_extruder_nr',
            'support_extruder_nr_layer_0',
            'support_interface_extruder_nr',
            'support_roof_extruder_nr',
            'support_bottom_extruder_nr',
            'support_type',
            'support_angle',
            'support_pattern',
            'support_wall_count',
            'zig_zaggify_support',
            'support_connect_zigzags',
            'support_infill_rate',
            'support_line_distance',
            'support_initial_layer_line_distance',
            'support_infill_angle',
            'support_brim_enable',
            'support_brim_width',
            'support_brim_line_count',
            'support_z_distance',
            'support_top_distance',
            'support_bottom_distance',
            'support_xy_distance',
            'support_xy_overrides_z',
            'support_xy_distance_overhang',
            'support_bottom_stair_step_height',
            'support_bottom_stair_step_width',
            'support_join_distance',
            'support_offset',
            'support_infill_sparse_thickness',
            'gradual_support_infill_steps',
            'gradual_support_infill_step_height',
            'minimum_support_area',
            'support_interface_enable',
            'support_roof_enable',
            'support_bottom_enable',
            'support_interface_height',
            'support_roof_height',
            'support_bottom_height',
            'support_interface_skip_height',
            'support_interface_density',
            'support_roof_density',
            'support_roof_line_distance',
            'support_bottom_density',
            'support_bottom_line_distance',
            'support_interface_pattern',
            'support_roof_pattern',
            'support_bottom_pattern',
            'minimum_interface_area',
            'minimum_roof_area',
            'minimum_bottom_area',
            'support_interface_offset',
            'support_roof_offset',
            'support_bottom_offset',
            'support_fan_enable',
            'support_supported_skin_fan_speed',
            'support_use_towers',
            'support_tower_diameter',
            'support_tower_maximum_supported_diameter',
            'support_tower_roof_angle',
            'support_mesh_drop_down',
        ]