# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Blackmagic(models.Model):

    print_sequence = models.ForeignKey(
        Choices,
        verbose_name='blackmagic',
        on_delete=models.SET_NULL,
    )

    infill_mesh = models.BooleanField(
        verbose_name="Infill Mesh",
    )

    infill_mesh_order = models.FloatField(
        verbose_name="Infill Mesh Order"
    )

    cutting_mesh = models.BooleanField(
        verbose_name="Cutting Mesh",
    )

    mold_enabled = models.BooleanField(
        verbose_name="Mold",
    )

    mold_width = models.FloatField(
        verbose_name="Minimal Mold Width"
    )

    mold_roof_height = models.FloatField(
        verbose_name="Mold Roof Height"
    )

    mold_angle = models.FloatField(
        verbose_name="Mold Angle"
    )

    support_mesh = models.BooleanField(
        verbose_name="Support Mesh",
    )

    anti_overhang_mesh = models.BooleanField(
        verbose_name="Anti Overhang Mesh",
    )

    magic_mesh_surface_mode = models.ForeignKey(
        Choices,
        verbose_name='blackmagic',
        on_delete=models.SET_NULL,
    )

    magic_spiralize = models.BooleanField(
        verbose_name="Spiralize Outer Contour",
    )

    smooth_spiralized_contours = models.BooleanField(
        verbose_name="Smooth Spiralized Contours",
    )

    relative_extrusion = models.BooleanField(
        verbose_name="Relative Extrusion",
    )


class BlackmagicForm(ModelForm):
    class Meta:
        model = Blackmagic
        fields = [
            'print_sequence',
            'infill_mesh',
            'infill_mesh_order',
            'cutting_mesh',
            'mold_enabled',
            'mold_width',
            'mold_roof_height',
            'mold_angle',
            'support_mesh',
            'anti_overhang_mesh',
            'magic_mesh_surface_mode',
            'magic_spiralize',
            'smooth_spiralized_contours',
            'relative_extrusion',
        ]