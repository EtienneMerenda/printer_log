# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Machine_Settings(models.Model):

    machine_name = models.CharField(
        verbose_name = "Machine Type",
        max_length=250,
    )

    machine_show_variants = models.BooleanField(
        verbose_name="Show Machine Variants",
    )

    machine_start_gcode = models.CharField(
        verbose_name = "Start G-code",
        max_length=250,
    )

    machine_end_gcode = models.CharField(
        verbose_name = "End G-code",
        max_length=250,
    )

    material_guid = models.CharField(
        verbose_name = "Material GUID",
        max_length=250,
    )

    material_diameter = models.FloatField(
        verbose_name="Diameter"
    )

    material_bed_temp_wait = models.BooleanField(
        verbose_name="Wait for Build Plate Heatup",
    )

    material_print_temp_wait = models.BooleanField(
        verbose_name="Wait for Nozzle Heatup",
    )

    material_print_temp_prepend = models.BooleanField(
        verbose_name="Include Material Temperatures",
    )

    material_bed_temp_prepend = models.BooleanField(
        verbose_name="Include Build Plate Temperature",
    )

    machine_width = models.FloatField(
        verbose_name="Machine Width"
    )

    machine_depth = models.FloatField(
        verbose_name="Machine Depth"
    )

    machine_shape = models.ForeignKey(
        Choices,
        verbose_name='machine_settings',
        on_delete=models.SET_NULL,
    )

    machine_buildplate_type = models.ForeignKey(
        Choices,
        verbose_name='machine_settings',
        on_delete=models.SET_NULL,
    )

    machine_height = models.FloatField(
        verbose_name="Machine Height"
    )

    machine_heated_bed = models.BooleanField(
        verbose_name="Has Heated Build Plate",
    )

    machine_center_is_zero = models.BooleanField(
        verbose_name="Is Center Origin",
    )

    machine_extruder_count = models.FloatField(
        verbose_name="Number of Extruders"
    )

    extruders_enabled_count = models.FloatField(
        verbose_name="Number of Extruders That Are Enabled"
    )

    machine_nozzle_tip_outer_diameter = models.FloatField(
        verbose_name="Outer Nozzle Diameter"
    )

    machine_nozzle_head_distance = models.FloatField(
        verbose_name="Nozzle Length"
    )

    machine_nozzle_expansion_angle = models.FloatField(
        verbose_name="Nozzle Angle"
    )

    machine_heat_zone_length = models.FloatField(
        verbose_name="Heat Zone Length"
    )

    machine_filament_park_distance = models.FloatField(
        verbose_name="Filament Park Distance"
    )

    machine_nozzle_temp_enabled = models.BooleanField(
        verbose_name="Enable Nozzle Temperature Control",
    )

    machine_nozzle_heat_up_speed = models.FloatField(
        verbose_name="Heat Up Speed"
    )

    machine_nozzle_cool_down_speed = models.FloatField(
        verbose_name="Cool Down Speed"
    )

    machine_min_cool_heat_time_window = models.FloatField(
        verbose_name="Minimal Time Standby Temperature"
    )

    machine_gcode_flavor = models.ForeignKey(
        Choices,
        verbose_name='machine_settings',
        on_delete=models.SET_NULL,
    )

    machine_firmware_retract = models.BooleanField(
        verbose_name="Firmware Retraction",
    )

    polygons


    polygons


    polygon


    polygon


    gantry_height = models.FloatField(
        verbose_name="Gantry Height"
    )

    machine_nozzle_id = models.CharField(
        verbose_name = "Nozzle ID",
        max_length=250,
    )

    machine_nozzle_size = models.FloatField(
        verbose_name="Nozzle Diameter"
    )

    machine_use_extruder_offset_to_offset_coords = models.BooleanField(
        verbose_name="Offset with Extruder",
    )

    extruder_prime_pos_z = models.FloatField(
        verbose_name="Extruder Prime Z Position"
    )

    extruder_prime_pos_abs = models.BooleanField(
        verbose_name="Absolute Extruder Prime Position",
    )

    machine_max_feedrate_x = models.FloatField(
        verbose_name="Maximum Speed X"
    )

    machine_max_feedrate_y = models.FloatField(
        verbose_name="Maximum Speed Y"
    )

    machine_max_feedrate_z = models.FloatField(
        verbose_name="Maximum Speed Z"
    )

    machine_max_feedrate_e = models.FloatField(
        verbose_name="Maximum Feedrate"
    )

    machine_max_acceleration_x = models.FloatField(
        verbose_name="Maximum Acceleration X"
    )

    machine_max_acceleration_y = models.FloatField(
        verbose_name="Maximum Acceleration Y"
    )

    machine_max_acceleration_z = models.FloatField(
        verbose_name="Maximum Acceleration Z"
    )

    machine_max_acceleration_e = models.FloatField(
        verbose_name="Maximum Filament Acceleration"
    )

    machine_acceleration = models.FloatField(
        verbose_name="Default Acceleration"
    )

    machine_max_jerk_xy = models.FloatField(
        verbose_name="Default X-Y Jerk"
    )

    machine_max_jerk_z = models.FloatField(
        verbose_name="Default Z Jerk"
    )

    machine_max_jerk_e = models.FloatField(
        verbose_name="Default Filament Jerk"
    )

    machine_steps_per_mm_x = models.FloatField(
        verbose_name="Steps per Millimeter (X)"
    )

    machine_steps_per_mm_y = models.FloatField(
        verbose_name="Steps per Millimeter (Y)"
    )

    machine_steps_per_mm_z = models.FloatField(
        verbose_name="Steps per Millimeter (Z)"
    )

    machine_steps_per_mm_e = models.FloatField(
        verbose_name="Steps per Millimeter (E)"
    )

    machine_endstop_positive_direction_x = models.BooleanField(
        verbose_name="X Endstop in Positive Direction",
    )

    machine_endstop_positive_direction_y = models.BooleanField(
        verbose_name="Y Endstop in Positive Direction",
    )

    machine_endstop_positive_direction_z = models.BooleanField(
        verbose_name="Z Endstop in Positive Direction",
    )

    machine_minimum_feedrate = models.FloatField(
        verbose_name="Minimum Feedrate"
    )

    machine_feeder_wheel_diameter = models.FloatField(
        verbose_name="Feeder Wheel Diameter"
    )


class Machine_SettingsForm(ModelForm):
    class Meta:
        model = Machine_Settings
        fields = [
            'machine_name',
            'machine_show_variants',
            'machine_start_gcode',
            'machine_end_gcode',
            'material_guid',
            'material_diameter',
            'material_bed_temp_wait',
            'material_print_temp_wait',
            'material_print_temp_prepend',
            'material_bed_temp_prepend',
            'machine_width',
            'machine_depth',
            'machine_shape',
            'machine_buildplate_type',
            'machine_height',
            'machine_heated_bed',
            'machine_center_is_zero',
            'machine_extruder_count',
            'extruders_enabled_count',
            'machine_nozzle_tip_outer_diameter',
            'machine_nozzle_head_distance',
            'machine_nozzle_expansion_angle',
            'machine_heat_zone_length',
            'machine_filament_park_distance',
            'machine_nozzle_temp_enabled',
            'machine_nozzle_heat_up_speed',
            'machine_nozzle_cool_down_speed',
            'machine_min_cool_heat_time_window',
            'machine_gcode_flavor',
            'machine_firmware_retract',
            'machine_disallowed_areas',
            'nozzle_disallowed_areas',
            'machine_head_polygon',
            'machine_head_with_fans_polygon',
            'gantry_height',
            'machine_nozzle_id',
            'machine_nozzle_size',
            'machine_use_extruder_offset_to_offset_coords',
            'extruder_prime_pos_z',
            'extruder_prime_pos_abs',
            'machine_max_feedrate_x',
            'machine_max_feedrate_y',
            'machine_max_feedrate_z',
            'machine_max_feedrate_e',
            'machine_max_acceleration_x',
            'machine_max_acceleration_y',
            'machine_max_acceleration_z',
            'machine_max_acceleration_e',
            'machine_acceleration',
            'machine_max_jerk_xy',
            'machine_max_jerk_z',
            'machine_max_jerk_e',
            'machine_steps_per_mm_x',
            'machine_steps_per_mm_y',
            'machine_steps_per_mm_z',
            'machine_steps_per_mm_e',
            'machine_endstop_positive_direction_x',
            'machine_endstop_positive_direction_y',
            'machine_endstop_positive_direction_z',
            'machine_minimum_feedrate',
            'machine_feeder_wheel_diameter',
        ]