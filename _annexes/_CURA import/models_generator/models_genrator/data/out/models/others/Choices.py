# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Choices(models.Model):

    computer = models.CharField(
        verbose_name = "Computer value",
        max_length=250,
    )

    human = models.CharField(
        verbose_name = "Human value",
        max_length=250,
    )


class ChoicesForm(ModelForm):
    class Meta:
        model = Choices
        fields = [
            'computer',
            'human',
        ]