# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class IntergerList(models.Model):

    integer_list = models.CharField(
        verbose_name = "Liste d'intégrales",
        max_length=250,
    )


class IntergerListForm(ModelForm):
    class Meta:
        model = IntergerList
        fields = [
            'integer_list',
        ]