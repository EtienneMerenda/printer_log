# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Experimental(models.Model):

    support_tree_enable = models.BooleanField(
        verbose_name="Tree Support",
    )

    support_tree_angle = models.FloatField(
        verbose_name="Tree Support Branch Angle"
    )

    support_tree_branch_distance = models.FloatField(
        verbose_name="Tree Support Branch Distance"
    )

    support_tree_branch_diameter = models.FloatField(
        verbose_name="Tree Support Branch Diameter"
    )

    support_tree_branch_diameter_angle = models.FloatField(
        verbose_name="Tree Support Branch Diameter Angle"
    )

    support_tree_collision_resolution = models.FloatField(
        verbose_name="Tree Support Collision Resolution"
    )

    support_tree_wall_thickness = models.FloatField(
        verbose_name="Tree Support Wall Thickness"
    )

    support_tree_wall_count = models.FloatField(
        verbose_name="Tree Support Wall Line Count"
    )

    slicing_tolerance = models.ForeignKey(
        Choices,
        verbose_name='experimental',
        on_delete=models.SET_NULL,
    )

    roofing_line_width = models.FloatField(
        verbose_name="Top Surface Skin Line Width"
    )

    roofing_pattern = models.ForeignKey(
        Choices,
        verbose_name='experimental',
        on_delete=models.SET_NULL,
    )

    roofing_angles = models.ForeignKey(
        IntegerList,
        verbose_name='experimental',
        on_delete=models.SET_NULL,
    )

    infill_enable_travel_optimization = models.BooleanField(
        verbose_name="Infill Travel Optimization",
    )

    material_flow_dependent_temperature = models.BooleanField(
        verbose_name="Auto Temperature",
    )

    material_flow_temp_graph = models.CharField(
        verbose_name = "Flow Temperature Graph",
        max_length=250,
    )

    minimum_polygon_circumference = models.FloatField(
        verbose_name="Minimum Polygon Circumference"
    )

    meshfix_maximum_resolution = models.FloatField(
        verbose_name="Maximum Resolution"
    )

    meshfix_maximum_travel_resolution = models.FloatField(
        verbose_name="Maximum Travel Resolution"
    )

    meshfix_maximum_deviation = models.FloatField(
        verbose_name="Maximum Deviation"
    )

    support_skip_some_zags = models.BooleanField(
        verbose_name="Break Up Support In Chunks",
    )

    support_skip_zag_per_mm = models.FloatField(
        verbose_name="Support Chunk Size"
    )

    support_zag_skip_count = models.FloatField(
        verbose_name="Support Chunk Line Count"
    )

    draft_shield_enabled = models.BooleanField(
        verbose_name="Enable Draft Shield",
    )

    draft_shield_dist = models.FloatField(
        verbose_name="Draft Shield X/Y Distance"
    )

    draft_shield_height_limitation = models.ForeignKey(
        Choices,
        verbose_name='experimental',
        on_delete=models.SET_NULL,
    )

    draft_shield_height = models.FloatField(
        verbose_name="Draft Shield Height"
    )

    conical_overhang_enabled = models.BooleanField(
        verbose_name="Make Overhang Printable",
    )

    conical_overhang_angle = models.FloatField(
        verbose_name="Maximum Model Angle"
    )

    coasting_enable = models.BooleanField(
        verbose_name="Enable Coasting",
    )

    coasting_volume = models.FloatField(
        verbose_name="Coasting Volume"
    )

    coasting_min_volume = models.FloatField(
        verbose_name="Minimum Volume Before Coasting"
    )

    coasting_speed = models.FloatField(
        verbose_name="Coasting Speed"
    )

    skin_alternate_rotation = models.BooleanField(
        verbose_name="Alternate Skin Rotation",
    )

    cross_infill_pocket_size = models.FloatField(
        verbose_name="Cross 3D Pocket Size"
    )

    cross_infill_density_image = models.CharField(
        verbose_name = "Cross Infill Density Image",
        max_length=250,
    )

    cross_support_density_image = models.CharField(
        verbose_name = "Cross Fill Density Image for Support",
        max_length=250,
    )

    spaghetti_infill_enabled = models.BooleanField(
        verbose_name="Spaghetti Infill",
    )

    spaghetti_infill_stepped = models.BooleanField(
        verbose_name="Spaghetti Infill Stepping",
    )

    spaghetti_max_infill_angle = models.FloatField(
        verbose_name="Spaghetti Maximum Infill Angle"
    )

    spaghetti_max_height = models.FloatField(
        verbose_name="Spaghetti Infill Maximum Height"
    )

    spaghetti_inset = models.FloatField(
        verbose_name="Spaghetti Inset"
    )

    spaghetti_flow = models.FloatField(
        verbose_name="Spaghetti Flow"
    )

    spaghetti_infill_extra_volume = models.FloatField(
        verbose_name="Spaghetti Infill Extra Volume"
    )

    support_conical_enabled = models.BooleanField(
        verbose_name="Enable Conical Support",
    )

    support_conical_angle = models.FloatField(
        verbose_name="Conical Support Angle"
    )

    support_conical_min_width = models.FloatField(
        verbose_name="Conical Support Minimum Width"
    )

    magic_fuzzy_skin_enabled = models.BooleanField(
        verbose_name="Fuzzy Skin",
    )

    magic_fuzzy_skin_thickness = models.FloatField(
        verbose_name="Fuzzy Skin Thickness"
    )

    magic_fuzzy_skin_point_density = models.FloatField(
        verbose_name="Fuzzy Skin Density"
    )

    magic_fuzzy_skin_point_dist = models.FloatField(
        verbose_name="Fuzzy Skin Point Distance"
    )

    flow_rate_max_extrusion_offset = models.FloatField(
        verbose_name="Flow rate compensation max extrusion offset"
    )

    flow_rate_extrusion_offset_factor = models.FloatField(
        verbose_name="Flow rate compensation factor"
    )

    wireframe_enabled = models.BooleanField(
        verbose_name="Wire Printing",
    )

    wireframe_height = models.FloatField(
        verbose_name="WP Connection Height"
    )

    wireframe_roof_inset = models.FloatField(
        verbose_name="WP Roof Inset Distance"
    )

    wireframe_printspeed = models.FloatField(
        verbose_name="WP Speed"
    )

    wireframe_printspeed_bottom = models.FloatField(
        verbose_name="WP Bottom Printing Speed"
    )

    wireframe_printspeed_up = models.FloatField(
        verbose_name="WP Upward Printing Speed"
    )

    wireframe_printspeed_down = models.FloatField(
        verbose_name="WP Downward Printing Speed"
    )

    wireframe_printspeed_flat = models.FloatField(
        verbose_name="WP Horizontal Printing Speed"
    )

    wireframe_flow = models.FloatField(
        verbose_name="WP Flow"
    )

    wireframe_flow_connection = models.FloatField(
        verbose_name="WP Connection Flow"
    )

    wireframe_flow_flat = models.FloatField(
        verbose_name="WP Flat Flow"
    )

    wireframe_top_delay = models.FloatField(
        verbose_name="WP Top Delay"
    )

    wireframe_bottom_delay = models.FloatField(
        verbose_name="WP Bottom Delay"
    )

    wireframe_flat_delay = models.FloatField(
        verbose_name="WP Flat Delay"
    )

    wireframe_up_half_speed = models.FloatField(
        verbose_name="WP Ease Upward"
    )

    wireframe_top_jump = models.FloatField(
        verbose_name="WP Knot Size"
    )

    wireframe_fall_down = models.FloatField(
        verbose_name="WP Fall Down"
    )

    wireframe_drag_along = models.FloatField(
        verbose_name="WP Drag Along"
    )

    wireframe_strategy = models.ForeignKey(
        Choices,
        verbose_name='experimental',
        on_delete=models.SET_NULL,
    )

    wireframe_straight_before_down = models.FloatField(
        verbose_name="WP Straighten Downward Lines"
    )

    wireframe_roof_fall_down = models.FloatField(
        verbose_name="WP Roof Fall Down"
    )

    wireframe_roof_drag_along = models.FloatField(
        verbose_name="WP Roof Drag Along"
    )

    wireframe_roof_outer_delay = models.FloatField(
        verbose_name="WP Roof Outer Delay"
    )

    wireframe_nozzle_clearance = models.FloatField(
        verbose_name="WP Nozzle Clearance"
    )

    adaptive_layer_height_enabled = models.BooleanField(
        verbose_name="Use Adaptive Layers",
    )

    adaptive_layer_height_variation = models.FloatField(
        verbose_name="Adaptive Layers Maximum Variation"
    )

    adaptive_layer_height_variation_step = models.FloatField(
        verbose_name="Adaptive Layers Variation Step Size"
    )

    adaptive_layer_height_threshold = models.FloatField(
        verbose_name="Adaptive Layers Threshold"
    )

    wall_overhang_angle = models.FloatField(
        verbose_name="Overhanging Wall Angle"
    )

    wall_overhang_speed_factor = models.FloatField(
        verbose_name="Overhanging Wall Speed"
    )

    bridge_settings_enabled = models.BooleanField(
        verbose_name="Enable Bridge Settings",
    )

    bridge_wall_min_length = models.FloatField(
        verbose_name="Minimum Bridge Wall Length"
    )

    bridge_skin_support_threshold = models.FloatField(
        verbose_name="Bridge Skin Support Threshold"
    )

    bridge_wall_coast = models.FloatField(
        verbose_name="Bridge Wall Coasting"
    )

    bridge_wall_speed = models.FloatField(
        verbose_name="Bridge Wall Speed"
    )

    bridge_wall_material_flow = models.FloatField(
        verbose_name="Bridge Wall Flow"
    )

    bridge_skin_speed = models.FloatField(
        verbose_name="Bridge Skin Speed"
    )

    bridge_skin_material_flow = models.FloatField(
        verbose_name="Bridge Skin Flow"
    )

    bridge_skin_density = models.FloatField(
        verbose_name="Bridge Skin Density"
    )

    bridge_fan_speed = models.FloatField(
        verbose_name="Bridge Fan Speed"
    )

    bridge_enable_more_layers = models.BooleanField(
        verbose_name="Bridge Has Multiple Layers",
    )

    bridge_skin_speed_2 = models.FloatField(
        verbose_name="Bridge Second Skin Speed"
    )

    bridge_skin_material_flow_2 = models.FloatField(
        verbose_name="Bridge Second Skin Flow"
    )

    bridge_skin_density_2 = models.FloatField(
        verbose_name="Bridge Second Skin Density"
    )

    bridge_fan_speed_2 = models.FloatField(
        verbose_name="Bridge Second Skin Fan Speed"
    )

    bridge_skin_speed_3 = models.FloatField(
        verbose_name="Bridge Third Skin Speed"
    )

    bridge_skin_material_flow_3 = models.FloatField(
        verbose_name="Bridge Third Skin Flow"
    )

    bridge_skin_density_3 = models.FloatField(
        verbose_name="Bridge Third Skin Density"
    )

    bridge_fan_speed_3 = models.FloatField(
        verbose_name="Bridge Third Skin Fan Speed"
    )

    clean_between_layers = models.BooleanField(
        verbose_name="Wipe Nozzle Between Layers",
    )

    max_extrusion_before_wipe = models.FloatField(
        verbose_name="Material Volume Between Wipes"
    )

    wipe_retraction_enable = models.BooleanField(
        verbose_name="Wipe Retraction Enable",
    )

    wipe_retraction_amount = models.FloatField(
        verbose_name="Wipe Retraction Distance"
    )

    wipe_retraction_extra_prime_amount = models.FloatField(
        verbose_name="Wipe Retraction Extra Prime Amount"
    )

    wipe_retraction_speed = models.FloatField(
        verbose_name="Wipe Retraction Speed"
    )

    wipe_retraction_retract_speed = models.FloatField(
        verbose_name="Wipe Retraction Retract Speed"
    )

    wipe_retraction_prime_speed = models.FloatField(
        verbose_name="Retraction Prime Speed"
    )

    wipe_pause = models.FloatField(
        verbose_name="Wipe Pause"
    )

    wipe_hop_enable = models.BooleanField(
        verbose_name="Wipe Z Hop When Retracted",
    )

    wipe_hop_amount = models.FloatField(
        verbose_name="Wipe Z Hop Height"
    )

    wipe_hop_speed = models.FloatField(
        verbose_name="Wipe Hop Speed"
    )

    wipe_brush_pos_x = models.FloatField(
        verbose_name="Wipe Brush X Position"
    )

    wipe_repeat_count = models.FloatField(
        verbose_name="Wipe Repeat Count"
    )

    wipe_move_distance = models.FloatField(
        verbose_name="Wipe Move Distance"
    )


class ExperimentalForm(ModelForm):
    class Meta:
        model = Experimental
        fields = [
            'support_tree_enable',
            'support_tree_angle',
            'support_tree_branch_distance',
            'support_tree_branch_diameter',
            'support_tree_branch_diameter_angle',
            'support_tree_collision_resolution',
            'support_tree_wall_thickness',
            'support_tree_wall_count',
            'slicing_tolerance',
            'roofing_line_width',
            'roofing_pattern',
            'roofing_angles',
            'infill_enable_travel_optimization',
            'material_flow_dependent_temperature',
            'material_flow_temp_graph',
            'minimum_polygon_circumference',
            'meshfix_maximum_resolution',
            'meshfix_maximum_travel_resolution',
            'meshfix_maximum_deviation',
            'support_skip_some_zags',
            'support_skip_zag_per_mm',
            'support_zag_skip_count',
            'draft_shield_enabled',
            'draft_shield_dist',
            'draft_shield_height_limitation',
            'draft_shield_height',
            'conical_overhang_enabled',
            'conical_overhang_angle',
            'coasting_enable',
            'coasting_volume',
            'coasting_min_volume',
            'coasting_speed',
            'skin_alternate_rotation',
            'cross_infill_pocket_size',
            'cross_infill_density_image',
            'cross_support_density_image',
            'spaghetti_infill_enabled',
            'spaghetti_infill_stepped',
            'spaghetti_max_infill_angle',
            'spaghetti_max_height',
            'spaghetti_inset',
            'spaghetti_flow',
            'spaghetti_infill_extra_volume',
            'support_conical_enabled',
            'support_conical_angle',
            'support_conical_min_width',
            'magic_fuzzy_skin_enabled',
            'magic_fuzzy_skin_thickness',
            'magic_fuzzy_skin_point_density',
            'magic_fuzzy_skin_point_dist',
            'flow_rate_max_extrusion_offset',
            'flow_rate_extrusion_offset_factor',
            'wireframe_enabled',
            'wireframe_height',
            'wireframe_roof_inset',
            'wireframe_printspeed',
            'wireframe_printspeed_bottom',
            'wireframe_printspeed_up',
            'wireframe_printspeed_down',
            'wireframe_printspeed_flat',
            'wireframe_flow',
            'wireframe_flow_connection',
            'wireframe_flow_flat',
            'wireframe_top_delay',
            'wireframe_bottom_delay',
            'wireframe_flat_delay',
            'wireframe_up_half_speed',
            'wireframe_top_jump',
            'wireframe_fall_down',
            'wireframe_drag_along',
            'wireframe_strategy',
            'wireframe_straight_before_down',
            'wireframe_roof_fall_down',
            'wireframe_roof_drag_along',
            'wireframe_roof_outer_delay',
            'wireframe_nozzle_clearance',
            'adaptive_layer_height_enabled',
            'adaptive_layer_height_variation',
            'adaptive_layer_height_variation_step',
            'adaptive_layer_height_threshold',
            'wall_overhang_angle',
            'wall_overhang_speed_factor',
            'bridge_settings_enabled',
            'bridge_wall_min_length',
            'bridge_skin_support_threshold',
            'bridge_wall_coast',
            'bridge_wall_speed',
            'bridge_wall_material_flow',
            'bridge_skin_speed',
            'bridge_skin_material_flow',
            'bridge_skin_density',
            'bridge_fan_speed',
            'bridge_enable_more_layers',
            'bridge_skin_speed_2',
            'bridge_skin_material_flow_2',
            'bridge_skin_density_2',
            'bridge_fan_speed_2',
            'bridge_skin_speed_3',
            'bridge_skin_material_flow_3',
            'bridge_skin_density_3',
            'bridge_fan_speed_3',
            'clean_between_layers',
            'max_extrusion_before_wipe',
            'wipe_retraction_enable',
            'wipe_retraction_amount',
            'wipe_retraction_extra_prime_amount',
            'wipe_retraction_speed',
            'wipe_retraction_retract_speed',
            'wipe_retraction_prime_speed',
            'wipe_pause',
            'wipe_hop_enable',
            'wipe_hop_amount',
            'wipe_hop_speed',
            'wipe_brush_pos_x',
            'wipe_repeat_count',
            'wipe_move_distance',
        ]