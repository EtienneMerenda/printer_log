# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Resolution(models.Model):

    layer_height = models.FloatField(
        verbose_name="Layer Height"
    )

    layer_height_0 = models.FloatField(
        verbose_name="Initial Layer Height"
    )

    line_width = models.FloatField(
        verbose_name="Line Width"
    )

    wall_line_width = models.FloatField(
        verbose_name="Wall Line Width"
    )

    wall_line_width_0 = models.FloatField(
        verbose_name="Outer Wall Line Width"
    )

    wall_line_width_x = models.FloatField(
        verbose_name="Inner Wall(s) Line Width"
    )

    skin_line_width = models.FloatField(
        verbose_name="Top/Bottom Line Width"
    )

    infill_line_width = models.FloatField(
        verbose_name="Infill Line Width"
    )

    skirt_brim_line_width = models.FloatField(
        verbose_name="Skirt/Brim Line Width"
    )

    support_line_width = models.FloatField(
        verbose_name="Support Line Width"
    )

    support_interface_line_width = models.FloatField(
        verbose_name="Support Interface Line Width"
    )

    support_roof_line_width = models.FloatField(
        verbose_name="Support Roof Line Width"
    )

    support_bottom_line_width = models.FloatField(
        verbose_name="Support Floor Line Width"
    )

    prime_tower_line_width = models.FloatField(
        verbose_name="Prime Tower Line Width"
    )

    initial_layer_line_width_factor = models.FloatField(
        verbose_name="Initial Layer Line Width"
    )


class ResolutionForm(ModelForm):
    class Meta:
        model = Resolution
        fields = [
            'layer_height',
            'layer_height_0',
            'line_width',
            'wall_line_width',
            'wall_line_width_0',
            'wall_line_width_x',
            'skin_line_width',
            'infill_line_width',
            'skirt_brim_line_width',
            'support_line_width',
            'support_interface_line_width',
            'support_roof_line_width',
            'support_bottom_line_width',
            'prime_tower_line_width',
            'initial_layer_line_width_factor',
        ]