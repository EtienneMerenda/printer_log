# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Dual(models.Model):

    prime_tower_enable = models.BooleanField(
        verbose_name="Enable Prime Tower",
    )

    prime_tower_size = models.FloatField(
        verbose_name="Prime Tower Size"
    )

    prime_tower_min_volume = models.FloatField(
        verbose_name="Prime Tower Minimum Volume"
    )

    prime_tower_position_x = models.FloatField(
        verbose_name="Prime Tower X Position"
    )

    prime_tower_position_y = models.FloatField(
        verbose_name="Prime Tower Y Position"
    )

    prime_tower_wipe_enabled = models.BooleanField(
        verbose_name="Wipe Inactive Nozzle on Prime Tower",
    )

    prime_tower_brim_enable = models.BooleanField(
        verbose_name="Prime Tower Brim",
    )

    ooze_shield_enabled = models.BooleanField(
        verbose_name="Enable Ooze Shield",
    )

    ooze_shield_angle = models.FloatField(
        verbose_name="Ooze Shield Angle"
    )

    ooze_shield_dist = models.FloatField(
        verbose_name="Ooze Shield Distance"
    )


class DualForm(ModelForm):
    class Meta:
        model = Dual
        fields = [
            'prime_tower_enable',
            'prime_tower_size',
            'prime_tower_min_volume',
            'prime_tower_position_x',
            'prime_tower_position_y',
            'prime_tower_wipe_enabled',
            'prime_tower_brim_enable',
            'ooze_shield_enabled',
            'ooze_shield_angle',
            'ooze_shield_dist',
        ]