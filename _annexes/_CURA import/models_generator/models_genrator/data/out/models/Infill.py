# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm


class Infill(models.Model):

    optional_extruder


    infill_sparse_density = models.FloatField(
        verbose_name="Infill Density"
    )

    infill_line_distance = models.FloatField(
        verbose_name="Infill Line Distance"
    )

    infill_pattern = models.ForeignKey(
        Choices,
        verbose_name='infill',
        on_delete=models.SET_NULL,
    )

    zig_zaggify_infill = models.BooleanField(
        verbose_name="Connect Infill Lines",
    )

    connect_infill_polygons = models.BooleanField(
        verbose_name="Connect Infill Polygons",
    )

    infill_angles = models.ForeignKey(
        IntegerList,
        verbose_name='infill',
        on_delete=models.SET_NULL,
    )

    infill_offset_x = models.FloatField(
        verbose_name="Infill X Offset"
    )

    infill_offset_y = models.FloatField(
        verbose_name="Infill Y Offset"
    )

    infill_multiplier = models.FloatField(
        verbose_name="Infill Line Multiplier"
    )

    infill_wall_line_count = models.FloatField(
        verbose_name="Extra Infill Wall Count"
    )

    sub_div_rad_add = models.FloatField(
        verbose_name="Cubic Subdivision Shell"
    )

    infill_overlap = models.FloatField(
        verbose_name="Infill Overlap Percentage"
    )

    infill_overlap_mm = models.FloatField(
        verbose_name="Infill Overlap"
    )

    skin_overlap = models.FloatField(
        verbose_name="Skin Overlap Percentage"
    )

    skin_overlap_mm = models.FloatField(
        verbose_name="Skin Overlap"
    )

    infill_wipe_dist = models.FloatField(
        verbose_name="Infill Wipe Distance"
    )

    infill_sparse_thickness = models.FloatField(
        verbose_name="Infill Layer Thickness"
    )

    gradual_infill_steps = models.FloatField(
        verbose_name="Gradual Infill Steps"
    )

    gradual_infill_step_height = models.FloatField(
        verbose_name="Gradual Infill Step Height"
    )

    infill_before_walls = models.BooleanField(
        verbose_name="Infill Before Walls",
    )

    min_infill_area = models.FloatField(
        verbose_name="Minimum Infill Area"
    )

    infill_support_enabled = models.BooleanField(
        verbose_name="Infill Support",
    )

    infill_support_angle = models.FloatField(
        verbose_name="Infill Overhang Angle"
    )

    skin_preshrink = models.FloatField(
        verbose_name="Skin Removal Width"
    )

    top_skin_preshrink = models.FloatField(
        verbose_name="Top Skin Removal Width"
    )

    bottom_skin_preshrink = models.FloatField(
        verbose_name="Bottom Skin Removal Width"
    )

    expand_skins_expand_distance = models.FloatField(
        verbose_name="Skin Expand Distance"
    )

    top_skin_expand_distance = models.FloatField(
        verbose_name="Top Skin Expand Distance"
    )

    bottom_skin_expand_distance = models.FloatField(
        verbose_name="Bottom Skin Expand Distance"
    )

    max_skin_angle_for_expansion = models.FloatField(
        verbose_name="Maximum Skin Angle for Expansion"
    )

    min_skin_width_for_expansion = models.FloatField(
        verbose_name="Minimum Skin Width for Expansion"
    )


class InfillForm(ModelForm):
    class Meta:
        model = Infill
        fields = [
            'infill_extruder_nr',
            'infill_sparse_density',
            'infill_line_distance',
            'infill_pattern',
            'zig_zaggify_infill',
            'connect_infill_polygons',
            'infill_angles',
            'infill_offset_x',
            'infill_offset_y',
            'infill_multiplier',
            'infill_wall_line_count',
            'sub_div_rad_add',
            'infill_overlap',
            'infill_overlap_mm',
            'skin_overlap',
            'skin_overlap_mm',
            'infill_wipe_dist',
            'infill_sparse_thickness',
            'gradual_infill_steps',
            'gradual_infill_step_height',
            'infill_before_walls',
            'min_infill_area',
            'infill_support_enabled',
            'infill_support_angle',
            'skin_preshrink',
            'top_skin_preshrink',
            'bottom_skin_preshrink',
            'expand_skins_expand_distance',
            'top_skin_expand_distance',
            'bottom_skin_expand_distance',
            'max_skin_angle_for_expansion',
            'min_skin_width_for_expansion',
        ]