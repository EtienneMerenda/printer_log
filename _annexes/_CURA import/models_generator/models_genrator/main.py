# -*- coding: utf-8 -*-

"""Script used for generate Models for printer_log Project"""

# Get tools
from bin.tools.saver import Saver
from bin.tools.template_maker import Template_maker

# Get context
from bin.context_maker.context_run import _data as data

# Others
import json
from pprint import pprint

# --- Save

saver = Saver()
# save current context
saver.save_file('data/out/JSON/extracted_context.json', data)
saver.save_file('data/tmp/choices.json', data['choices'])
saver.save_file('data/tmp/context.json', data['context'])

# --- Create each CURA table

tm = Template_maker(saver)

for table in data['context']:

    if table != 'command_line_settings':

        context = data['context'][table]
        context['table_name'] = table.title()

        tm.templating(f'data/out/models/{table.title()}.py', context)


# --- Adding others tables used for Database optimisation

    # Choice.py
    # IntList.py

with open('./data/in/choices_context.json') as file:
    context = json.load(file)

for table_name, context in context.items():
    tm.templating(f'data/out/models/others/{table_name.title()}.py', context)
