# -*- coding: utf-8 -*-
import json
import os

class Saver():

    def __init__(self):
        pass

    def create_folder(self, path):
        '''Take path and create needed folders'''
        if '/' in path:
            path_ = path.split('/')
            # Delete file name
            if '.' in path_[-1]:
                path_.pop()
            f_path = ''

            # Create each folder
            for f in path_:
                f_path += f'{f}/'
                try:
                    os.mkdir(f_path)
                except OSError as e:
                    pass

    def save_file(self, path, object):
        '''Save object in path with folder creation'''

        # Create folders
        self.create_folder(path)

        # If file is json, it's dump
        if path.split('.')[-1] == 'json':

            with open(path, 'w', encoding='utf-8') as file:
                json.dump(object, file, indent=4)

        # else is write
        else:
            with open(path, 'w', encoding='utf-8') as file:
                file.write(object.encode('latin-1').decode('utf-8'))
