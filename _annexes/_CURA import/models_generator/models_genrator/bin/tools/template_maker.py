# -*- coding: utf-8 -*-
import jinja2

class Template_maker():
    """Class used for generating models for Django about Cura settings"""

    def __init__(self, saver):
        self.saver = saver

    def jinja_setup(self, context):

        # Indicate where are templates
        template_loader = jinja2.FileSystemLoader(
            # setting template search path
            searchpath=["./etc/snippets/model", "./etc/snippets/field"]
        )

        # Indicate which environnment and setup jinja2
        template_env = jinja2.Environment(
            loader=template_loader,
            # Remove trailing newlins
            lstrip_blocks=True,
            trim_blocks=True
        )

        # Define wich base is used

        if len(context) > 1:
            TEMPLATE_FILE = "model.py"
            self.template = template_env.get_template(TEMPLATE_FILE)

        else:
            TEMPLATE_FILE = "model_whitout_loop.py"
            self.template = template_env.get_template(TEMPLATE_FILE)


    def template_print(self, context):

        self.jinja_setup(context)
        print(self.template.render(context))

    def templating(self, file_path, context):

        self.jinja_setup(context)
        self.saver.save_file(file_path, self.template.render(context))
