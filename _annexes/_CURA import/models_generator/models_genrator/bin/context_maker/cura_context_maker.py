# -*- coding: utf-8 -*-

class Context_maker():
    """Used for create context maker for CURA json"""

    def __init__(self, json_):

        self.json_ = json_

        # used to generate context
        self.data = {
            'context': {},
            'choices': []
            }

        self.contextList = []


        self.heritage = []

        # keys we want get in json
        self.expected_keys = []

        # Counter of floor
        self.floor_count = 0

        # Bool
        self.choices = False # Used to define if table include fields with choice

    def set_expected_keys(self, expected_keys):
        """Method used to set expeceted_keys"""

        self.expected_keys = expected_keys

    def purge_contextList(self):

        self.contextList = []
        self.choices = False

    def get_table(self):

        for key in self.json_['settings']:
            self.data['context'][key] = {
                'label': key,
                'fields': [],
                'choices': False
                }

    def children_detector(self, key, json_):
        """Check if 'children' is in key and call get_context if it is"""

        if 'children' in json_.keys():

            # count each level of json
            self.floor_count += 1

            # add key in heritage list
            self.heritage.append(key)

            # Recursive call of get_context method
            self.get_context(json_['children'])

            # Up one level
            self.floor_count -= 1
            # Finaly remove heritage key
            self.heritage.pop()

    def get_context(self, json_:"json_['children']"):
        """Get expected keys of part of json"""

        for key in json_:
            context = {}

            # get key to namify field of table
            context['field_name'] = key

            for label_data, data in json_[key].items():

                # get value of keys in expected_keys list
                if label_data in self.expected_keys:
                    context[label_data] = data

                    if label_data == 'options':
                        # Set boolean in True
                        self.choices = True

                        for machine_readable, human_readable in data.items():
                            self.data['choices'].append(
                                (machine_readable, human_readable)
                                )

            # set heritage if is it
            if len(self.heritage) > 0:
                context['heritage'] = ' > '.join(self.heritage)

            # append temp list each loop
            self.contextList.append(context)

            # Check if there is children in keys and recursive call trought get_children() method
            self.children_detector(key ,json_[key])


    def run(self):

        self.get_table()

        for table_name in self.data['context'].keys():
            # loop in each table
            self.purge_contextList()

            # Fill fields key with data
            self.get_context(self.json_['settings'][table_name]['children'])

            # Bool indicate which table have choices
            self.data['context'][table_name]['choices'] = self.choices

            # append fields each table loop
            self.data['context'][table_name]['fields'] = self.contextList

        return self.data
