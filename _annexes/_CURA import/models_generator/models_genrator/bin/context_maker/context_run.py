# -*- coding: utf-8 -*-

from bin.context_maker.cura_context_maker import Context_maker
import jinja2, json

# Load json
with open('../fdmprinter.def.json', 'r', encoding='latin-1') as file:
    cura_json = json.load(file)

# Parse json and get wanted keys
c_maker = Context_maker(json_=cura_json)
c_maker.set_expected_keys(
    ['label',
    'decription',
    'type',
    'enabled',
    'unit',
    'options'
    ]
)

_data = c_maker.run()

with open('data/out/cura_settings_work_in_progess.txt', 'w') as file:
    for key, data in _data['context'].items():
        file.write(f'{key}:\n')
        for key_, data_ in data.items():
            if not isinstance(data_, list):
                file.write(f'   {key_}: {data_}\n')
            else:
                for dict_ in data_:
                    for key__, data__ in dict_.items():
                        file.write(f'       {key__}: {data__}\n')

                    file.write('\n')
