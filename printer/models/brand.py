from django.db import models
from django.forms import ModelForm


class Brand(models.Model):

    brand = models.CharField(
        max_length=200,
        verbose_name='Marque'
        )


class BrandForm(ModelForm):
    class Meta:
        model = Brand
        fields = ['brand']


class Version(models.Model):

    model = models.CharField(
        max_length=200,
        verbose_name='Modèle'
        )


class ModelForm_(ModelForm):
    class Meta:
        model = Version
        fields = ['model']
