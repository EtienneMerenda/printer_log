# import models for create table in PostGresSQL Database (Django implementation)
from django.db import models
# import ModelForm for create form used in table in post method.
from django.forms import ModelForm
# import Extruder for link many extruder on user's printer in case of several extruder
from .extruder import Extruder
from .brand import Brand, Version


class Printer(models.Model):
    """Table used to register printer model and specification"""

    name = models.CharField(
            max_length=100,
            verbose_name="Nom de l'imprimante",
    )

    brand = models.ForeignKey(
            Brand,
            max_length=100,
            verbose_name="Marque de l'imprimante",
            on_delete=models.SET_NULL,
            null=True
    )

    model = models.ForeignKey(
            Version,
            verbose_name="Modèle de l'imprimante",
            on_delete=models.SET_NULL,
            null=True
    )


    # many to many rel to choice more one extruder
    extruder = models.ManyToManyField(
            Extruder,
            verbose_name='Extrudeuse'
    )

    print_x = models.IntegerField(
        verbose_name='Largeur de la zone d\'impression')

    print_y = models.IntegerField(
        verbose_name='Profondeur de la zone d\'impression')

    print_z = models.IntegerField(
        verbose_name='Hauteur de la zone d\'impression')


class PrinterFrom(ModelForm):
    class Meta:
        model = Printer
        fields = [
            'name',
            'brand',
            'model',
            'extruder',
            'print_x',
            'print_y',
            'print_z'
        ]
