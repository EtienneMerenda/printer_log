from django.db import models
# import ModelForm to create Form as of Model class
from django.forms import ModelForm
from tools.float_range import f_range


class Extruder(models.Model):
    nozzle_sizes = [i for i in f_range(0, 2, 0.1)]

    name = models.CharField(
            max_length=100,
            verbose_name='Nom de l\'extrudeur'
    )

    number = models.IntegerField(
            verbose_name='Numéro de l\'extrudeur'
    )

    nozzle_size = models.FloatField(
            verbose_name='Diamètre de l\'extrudeur',
            choices=((str(i), str(i)) for i in nozzle_sizes)
    )

    start_code = models.TextField(
            verbose_name='G-code de départ de l\'extrudeur'
    )

class ExtruderForm(ModelForm):
    class Meta:
        model = Extruder
        fields = [
                'name',
                'number',
                'nozzle_size',
                'start_code'
        ]
