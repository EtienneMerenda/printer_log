from django.contrib import admin
from .models import Brand, Extruder, Version, Printer

# Register your models here.
admin.site.register(Brand)
admin.site.register(Extruder)
admin.site.register(Version)
admin.site.register(Printer)
