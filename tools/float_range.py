# -*- coding: utf-8 -*-

def f_range(start, end, step):
    """custom range work with float number"""
    while start <= end:
        start += step
        yield round(start, 1)
